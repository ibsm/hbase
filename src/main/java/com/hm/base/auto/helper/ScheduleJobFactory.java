package com.hm.base.auto.helper;

import org.quartz.spi.TriggerFiredBundle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.scheduling.quartz.AdaptableJobFactory;
import org.springframework.stereotype.Component;

/**
 * @author shishun.wang
 * @date 上午10:45:28 2017年8月24日
 * @version 1.0
 * @describe 解决spring不能在quartz中注入bean的问题
 */
@Component
public class ScheduleJobFactory extends AdaptableJobFactory {

	@Autowired
	private AutowireCapableBeanFactory capableBeanFactory;

	@Override
	protected Object createJobInstance(TriggerFiredBundle bundle) throws Exception {
		Object jobInstance = super.createJobInstance(bundle);
		capableBeanFactory.autowireBean(jobInstance); // 这一步解决不能spring注入bean的问题
		return jobInstance;
	}

}
