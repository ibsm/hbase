/**
 * 
 */
package com.hm.base.auto.helper;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * @author shishun.wang
 * @date 下午2:12:58 2017年5月26日
 * @version 1.0
 * @describe
 */
@Slf4j
@Component
public class HmApplicationListenerStart implements ApplicationListener<ContextRefreshedEvent> {

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		log.info("springboot 初始化完成...");

		// SpringBeanUtil.getBean(LocalDatabaseStoreTaskHandler.class).doTask();
		// SpringBeanUtil.getBean(MyTaskHandler.class).doTask();

	}

}
