package com.hm.base.auto.helper;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hm.base.auto.su.ControllerResult;
import com.hm.common.exception.ErrorCode;
import com.hm.common.exception.ServiceException;

import lombok.extern.slf4j.Slf4j;

/**
 * @author shishun.wang
 * @date 上午11:17:32 2017年6月5日
 * @version 1.0
 * @describe
 */
@Slf4j
@ControllerAdvice
@ResponseBody
public class GlobalExceptionHandler {

	@ExceptionHandler(value = Exception.class)
	public Object serviceExceptionHandler(HttpServletRequest request, Exception e) throws Exception {
		log.error(e.getMessage(), e);
		if (e.toString().contains(ServiceException.class.getName())) {
			String[] execMessage = e.getMessage().trim().split("\\:\\*\\:");
			if (execMessage.length > 1) {
				return ControllerResult.failed(execMessage[0], execMessage[1]);
			}
			return ControllerResult.failed(execMessage[0]);
		}
		return ControllerResult.failed(ErrorCode.SYSTEM_ERROR.describe());
	}
}
