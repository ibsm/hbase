package com.hm.base.auto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import com.hm.base.auto.su.R.Restful;
import com.hm.base.websocket.LocalRunningInstanceLog;

/**
 * @author shishun.wang
 * @date 下午3:27:56 2017年9月11日
 * @version 1.0
 * @describe
 */
@Configuration
public class WebSocketConfigutation implements WebSocketConfigurer {

	@Autowired
	private LocalRunningInstanceLog localRunningInstanceLog;

	@Override
	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
		registry.addHandler(localRunningInstanceLog, Restful.API + "/instance/local/log");
	}

}
