package com.hm.base.auto.su;

/**
 * @author shishun.wang
 * @date 下午2:59:29 2017年6月2日
 * @version 1.0
 * @describe
 */
public interface R {

	/**
	 * @author shishun.wang
	 * @date 下午3:00:57 2017年6月2日
	 * @version 1.0
	 * @describe
	 */
	public static interface Restful {

		public static String API = "/restful/base/" + R.API.API_VERSION;

		public static String REMOTE_CLIENT_AUTHORIZATION = "Authorization";

		public static String REMOTE_CLIENT_AUTHORIZATION_DEV = "DEV_AUTHORIZATION";
	}

	/**
	 * @author shishun.wang
	 * @date 下午3:21:30 2017年6月5日
	 * @version 1.0
	 * @describe
	 */
	public static interface API {

		public static String API_TITLE = "基础数据管理";

		public static String API_VERSION = "v1.0";

		public static String API_DESCRIBE = "仅限内部使用:除了登陆外其他接口请求参数header中必须携带有身份认证参数,Authorization=登陆成功返回token值";
	}

	/**
	 * @author shishun.wang
	 * @date 下午3:22:02 2017年6月5日
	 * @version 1.0
	 * @describe
	 */
	public static interface RedisKey {

		public String SUBSCRIBER_LOGIN_GROUP = "hm:login:subscriber:";

		public String SCHEDULE_REFRESH_TRIGGER_GROUP = "hm:basic:schedule:refresh:trigger";

		public String WEB_VERIFICATION_CODE_GROUP = "hm:login:subscriber:";

		/**
		 * @author shishun.wang
		 * @date 下午6:19:36 2017年8月31日
		 * @version 1.0
		 * @describe
		 */
		public static interface RedisKeySeEx {

			public int WEB_VERIFICATION_CODE_GROUP = 5 * 60;// 5 分钟
		}
	}

	/**
	 * @author shishun.wang
	 * @date 下午3:29:02 2017年6月16日
	 * @version 1.0
	 * @describe
	 */
	public static interface QiNiuKey {

		public String COMMON_UPLOAD = "common_upload";
	}

}
