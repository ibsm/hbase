package com.hm.base.auto.su;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * @author shishun.wang
 * @date 上午11:46:51 2017年5月27日
 * @version 1.0
 * @describe
 */
public interface HmMapper<T> extends Mapper<T>, MySqlMapper<T> {

}
