package com.hm.base.auto.su;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hm.common.su.bean.ServerResponse;

/**
 * @author shishun.wang
 * @date 下午3:12:21 2017年6月2日
 * @version 1.0
 * @describe
 */
public class ControllerResult<T> {

	public static <T> ResponseEntity<ServerResponse<T>> success(T result) {
		return new ResponseEntity<ServerResponse<T>>(new ServerResponse<T>().success(result), HttpStatus.OK);
	}

	public static <T> ResponseEntity<ServerResponse<T>> failed(String message) {
		return new ResponseEntity<ServerResponse<T>>(new ServerResponse<T>().failure(message), HttpStatus.BAD_REQUEST);
	}

	public static <T> ResponseEntity<ServerResponse<T>> failed(Exception e) {
		return new ResponseEntity<ServerResponse<T>>(new ServerResponse<T>().failure(e.getMessage()),
				HttpStatus.BAD_REQUEST);
	}

	public static <T> ResponseEntity<ServerResponse<T>> failed(String code, String describe) {
		return new ResponseEntity<ServerResponse<T>>(new ServerResponse<T>().failure(code, describe),
				HttpStatus.BAD_REQUEST);
	}

}
