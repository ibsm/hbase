/**
 * 
 */
package com.hm.base.auto;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

/**
 * @author shishun.wang
 * @date 上午11:44:44 2017年5月26日
 * @version 1.0
 * @describe
 */

@Data
@ConfigurationProperties(prefix = "com.hm.base.init")
public class SystemEnvConfigProperties {

	/**
	 * 是否开启开发模式
	 */
	private boolean devModel;

	/**
	 * 开发模式默认用户id
	 */
	private long devModelUser;

	/**
	 * 是否保护系统初始化用户
	 */
	private boolean protectInitUser;

	/**
	 * 是否开启组织机构权限模式
	 */
	private boolean openOrganization;
	
	/**
	 * 开启请求日志记录
	 */
	private boolean openReqLogRecord;

	/**
	 * 刷新调度时间频率
	 */
	private String refreshScheduleJobsTask;

	private String zipFilePassword;
	
	/**
	 * 后台项目存放目录
	 */
	private String serverInstancePath;
	
	/**
	 * 开启服务器实例日志输出
	 */
	private boolean openServerInstanceLogOutput;

	private String dbBusWebUsername;

	private String dbBusWebPassword;

	private String qiniuPrefixUri;

	private String qiniuAccessKey;

	private String qiniuSecretKey;

	private BackstageAccreditInfo backstageAccreditInfo = new BackstageAccreditInfo();
	
	private RedisConfig redisConfig = new RedisConfig();

	private DatabaseBackUp databaseBackUp = new DatabaseBackUp();
	
	@Data
	public class DatabaseBackUp {

		/**
		 * 数据库备份文件路径
		 */
		private String backUpFilePath;

		private String host;

		private int port;

		private String username;

		private String password;
		
		private String charset;
		
		private boolean open;
	}

	@Data
	public class BackstageAccreditInfo {

		private String version;

		private String validity;

		private String officialWebsite;

		private String officialWebsiteUri;
	}

	@Data
	public class RedisConfig {

		private String host;

		private int port;

		private String password;

		private int timeout;

		private RedisPool pool = new RedisPool();

		@Data
		public class RedisPool {

			/**
			 * 可用连接实例的最大数目,如果赋值为-1,表示不限制.
			 */
			private int maxTotal;

			/**
			 * 控制一个Pool最多有多少个状态为idle(空闲的)jedis实例,默认值也是8
			 */
			private int maxIdle;

			/**
			 * 等待可用连接的最大时间,单位毫秒,默认值为-1,表示永不超时/如果超过等待时间,则直接抛出异常
			 */
			private int maxWaitMillis;

			/**
			 * 在borrow一个jedis实例时,是否提前进行validate操作,如果为true,则得到的jedis实例均是可用的
			 */
			private boolean testOnBorrow;
		}
	}
}
