package com.hm.base.auto;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.hm.base.auto.su.R.API;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author shishun.wang
 * @date 下午2:11:16 2017年5月26日
 * @version 1.0
 * @describe
 */
@Configuration
@EnableSwagger2
@Profile({ "dev", "test" })
public class HmSwagger2Configuration {

	@Bean
	public Docket swaggerSpringMvcPlugin() {
		ApiInfo apiInfo = new ApiInfoBuilder().title(API.API_TITLE).description(API.API_DESCRIBE)
				.version(API.API_VERSION).build();

		return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo).select()
				.apis(RequestHandlerSelectors.basePackage("com.hm.base.api")).paths(PathSelectors.any()).build();
	}
}
