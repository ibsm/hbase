package com.hm.base.job.task;

import org.springframework.stereotype.Component;

import com.hm.base.job.su.ScheduleJobTaskSupport;

import lombok.extern.slf4j.Slf4j;

/**
 * @author shishun.wang
 * @date 下午5:00:51 2017年9月19日
 * @version 1.0
 * @describe 
 */
@Slf4j
@Component
public class MyTaskHandler implements ScheduleJobTaskSupport{

	@Override
	public void doTask() {
		log.info("定时任务打印数据{}", System.currentTimeMillis());
	}

}
