package com.hm.base.job.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hm.base.auto.SystemEnvConfigProperties;
import com.hm.base.job.su.ScheduleJobTaskSupport;
import com.hm.common.network.ssh2.RemoteSSH2Factory;
import com.hm.common.util.EncryptUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * @author shishun.wang
 * @date 下午5:21:33 2017年9月19日
 * @version 1.0
 * @describe
 */
@Slf4j
@Component
public class LocalDatabaseStoreTaskHandler implements ScheduleJobTaskSupport {

	@Autowired
	private SystemEnvConfigProperties systemEnvConfigProperties;

	@Override
	public void doTask() {
		// 122.114.176.221
		if(!systemEnvConfigProperties.getDatabaseBackUp().isOpen()){
			return;
		}
		
		RemoteSSH2Factory tool = new RemoteSSH2Factory(systemEnvConfigProperties.getDatabaseBackUp().getHost(),
				systemEnvConfigProperties.getDatabaseBackUp().getPort(),
				systemEnvConfigProperties.getDatabaseBackUp().getUsername(),
				new String(EncryptUtil.Base64
						.decode(systemEnvConfigProperties.getDatabaseBackUp().getPassword().toCharArray())),
				systemEnvConfigProperties.getDatabaseBackUp().getCharset());
		String result = tool.exec("/data/dbdata/backup_mysql.sh");
		log.info("{}执行数据库远程备份:{}", System.currentTimeMillis(), result);
	}

}
