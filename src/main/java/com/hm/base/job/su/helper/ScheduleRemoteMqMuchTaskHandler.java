package com.hm.base.job.su.helper;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.hm.base.job.su.ScheduleRemoteMqPushTaskSupport;
import com.hm.base.vo.ScheduleTaskCallResultVo;
import com.hm.base.vo.ScheduleTaskVo;
import com.hm.common.util.CommonUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * @author shishun.wang
 * @date 上午10:44:21 2017年9月13日
 * @version 1.0
 * @describe
 */
@Slf4j
@Component
public class ScheduleRemoteMqMuchTaskHandler implements ScheduleRemoteMqPushTaskSupport {

	@Autowired
	private AmqpTemplate rabbitTemplate;

	@Override
	public ScheduleTaskCallResultVo pushTask(ScheduleTaskVo scheduleTaskVo) {
		Object receive = this.rabbitTemplate.convertSendAndReceive(scheduleTaskVo.getScheduleJdobGroup(), null,
				scheduleTaskVo.getTaskIdentifyToken());
		if (CommonUtil.isEmpty(receive)) {
			log.error("定时远程消息:{},发送失败.原因：返回值为空", scheduleTaskVo.getScheduleJdobGroup());
			return null;
		}
		
		try {
			ScheduleTaskCallResultVo taskCallResultVo = JSON.parseObject(receive.toString(), ScheduleTaskCallResultVo.class);
			
			if (!taskCallResultVo.isSuccessful()) {
				log.error("定时远程消息,发送失败,原因:{}", taskCallResultVo);
			}
			
			return taskCallResultVo;
		} catch (Exception e) {
			log.error("定时远程消息,发送失败:{}", e);
		}
		return null;
	}

}
