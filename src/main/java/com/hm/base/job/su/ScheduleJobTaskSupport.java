package com.hm.base.job.su;

/**
 * @author shishun.wang
 * @date 下午5:00:06 2017年9月19日
 * @version 1.0
 * @describe 
 */
public interface ScheduleJobTaskSupport {

	public void doTask();
}
