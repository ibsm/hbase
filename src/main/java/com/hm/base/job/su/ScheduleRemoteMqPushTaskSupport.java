package com.hm.base.job.su;

import com.hm.base.vo.ScheduleTaskCallResultVo;
import com.hm.base.vo.ScheduleTaskVo;

/**
 * @author shishun.wang
 * @date 上午10:48:43 2017年9月13日
 * @version 1.0
 * @describe
 */
public interface ScheduleRemoteMqPushTaskSupport {

	public ScheduleTaskCallResultVo pushTask(ScheduleTaskVo scheduleTaskVo);
}
