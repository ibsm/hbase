package com.hm.base.job;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hm.base.job.su.helper.ScheduleRemoteMqMuchTaskHandler;
import com.hm.base.vo.ScheduleTaskVo;
import com.hm.common.util.CommonUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * @author shishun.wang
 * @date 上午11:17:54 2017年9月12日
 * @version 1.0
 * @describe 群推消息，所有订阅者收到消息一致
 */
@Slf4j
@Component("scheduleRemoteMqMuchTask")
public class ScheduleRemoteMqMuchTask implements Job {

	@Autowired
	private ScheduleRemoteMqMuchTaskHandler scheduleRemoteMqMuchTaskHandler;

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		if (CommonUtil.isEmpty(context)) {
			return;
		}

		CronTrigger trigger = (CronTrigger) context.getTrigger();
		String groupName = trigger.getKey().getGroup();

		String msgToken = groupName + "_ScheduleRemoteMqMuchTask_" + System.currentTimeMillis();
		ScheduleTaskVo scheduleTaskVo = new ScheduleTaskVo();
		{
			scheduleTaskVo.setScheduleJdobGroup(groupName);
			scheduleTaskVo.setTaskIdentifyToken(msgToken);
		}

		log.info("推送消息给所有订阅者:{}", scheduleTaskVo);
		scheduleRemoteMqMuchTaskHandler.pushTask(scheduleTaskVo);
	}

}
