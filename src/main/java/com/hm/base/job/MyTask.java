package com.hm.base.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hm.base.job.task.MyTaskHandler;

/**
 * @author shishun.wang
 * @date 下午2:05:57 2017年8月24日
 * @version 1.0
 * @describe
 */
@Component("myTask")
public class MyTask implements Job {

	@Autowired
	private MyTaskHandler myTaskHandler;
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		myTaskHandler.doTask();
	}

}
