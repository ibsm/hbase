package com.hm.base.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hm.base.job.task.LocalDatabaseStoreTaskHandler;

/**
 * @author shishun.wang
 * @date 上午10:22:35 2017年8月30日
 * @version 1.0
 * @describe
 */
@Component("localDatabaseStoreTask")
public class LocalDatabaseStoreTask implements Job {

	@Autowired
	private LocalDatabaseStoreTaskHandler localDatabaseStoreTaskHandler;
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		localDatabaseStoreTaskHandler.doTask();
	}

}
