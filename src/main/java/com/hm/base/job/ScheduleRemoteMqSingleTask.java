package com.hm.base.job;

import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hm.base.job.su.helper.ScheduleRemoteMqSingleTaskHandler;
import com.hm.base.vo.ScheduleTaskVo;
import com.hm.common.util.CommonUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * @author shishun.wang
 * @date 下午3:55:41 2017年9月7日
 * @version 1.0
 * @describe 推送消息单个服务器能收到消息，谁先得到算谁的
 */
@Slf4j
@Component("scheduleRemoteMqSingleTask")
public class ScheduleRemoteMqSingleTask implements Job {

	@Autowired
	private ScheduleRemoteMqSingleTaskHandler scheduleRemoteMqSingleTaskHandler;

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		if (CommonUtil.isEmpty(context)) {
			return;
		}
		CronTrigger trigger = (CronTrigger) context.getTrigger();
		String groupName = trigger.getKey().getGroup();

		String msgToken = groupName + "_ScheduleRemoteMqSingleTask_" + System.currentTimeMillis();
		ScheduleTaskVo scheduleTaskVo = new ScheduleTaskVo();
		{
			scheduleTaskVo.setScheduleJdobGroup(groupName);
			scheduleTaskVo.setTaskIdentifyToken(msgToken);
		}

		log.info("推送单实例消息:{}", scheduleTaskVo);
		scheduleRemoteMqSingleTaskHandler.pushTask(scheduleTaskVo);
	}

}
