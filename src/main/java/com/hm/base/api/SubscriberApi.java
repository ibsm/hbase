package com.hm.base.api;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hm.base.auto.helper.HmSessionFactory;
import com.hm.base.auto.su.ControllerResult;
import com.hm.base.auto.su.R.Restful;
import com.hm.base.service.SubscriberService;
import com.hm.base.vo.SubscriberVo;
import com.hm.common.su.bean.PageInfo;
import com.hm.common.su.bean.PageInfo.PageParam;
import com.hm.common.su.bean.ServerResponse;
import com.hm.common.util.RemoteClientUtil;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * @author shishun.wang
 * @date 下午2:57:21 2017年6月2日
 * @version 1.0
 * @describe 系统登录账户信息管理
 */
@Slf4j
@ResponseBody
@RestController
@RequestMapping(Restful.API + "/subscriber")
public class SubscriberApi {

	private static final String SWWAGER_TAG = "系统账户管理";

	@Autowired
	private SubscriberService subscriberService;

	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(tags = SWWAGER_TAG, value = "新增用户信息", httpMethod = "POST", notes = "新增用户信息,返回添加是否成功")
	public ResponseEntity<ServerResponse<Boolean>> addSubscriber(
			@ApiParam(required = true, name = "body") @RequestBody(required = true) SubscriberVo subscriberVo,
			HttpServletRequest request) {
		log.debug("用户{}新增用户信息", HmSessionFactory.currentUserId());
		subscriberVo.setLostLoginIp(RemoteClientUtil.getHost(request));
		subscriberService.addSubscriber(subscriberVo);
		return ControllerResult.success(true);
	}

	@RequestMapping(value = "/operator", method = RequestMethod.GET)
	@ApiOperation(tags = SWWAGER_TAG, value = "获取当前登录用户基本信息", httpMethod = "GET", notes = "返回当前登录用户基本信息")
	public ResponseEntity<ServerResponse<SubscriberVo>> loadCurrentLoginSubscriber() {
		log.debug("加载用户{},基本信息", HmSessionFactory.currentUserId());
		return ControllerResult.success(subscriberService.getSubscriber(HmSessionFactory.currentUserId()));
	}

	@RequestMapping(value = "/{subscriberId}", method = RequestMethod.GET)
	@ApiOperation(tags = SWWAGER_TAG, value = "获取指定用户基本信息", httpMethod = "GET", notes = "根据用户id返回指定用户基本信息")
	public ResponseEntity<ServerResponse<SubscriberVo>> loadSubscriberById(
			@ApiParam(required = true, name = "subscriberId", value = "用户ID") @PathVariable("subscriberId") Long subscriberId) {
		log.debug("用户{},获取用户{}基本信息", HmSessionFactory.currentUserId(), subscriberId);
		return ControllerResult.success(subscriberService.getSubscriber(subscriberId));
	}

	@RequestMapping(value = "/{subscriberId}", method = RequestMethod.PUT)
	@ApiOperation(tags = SWWAGER_TAG, value = "修改指定用户基本信息", httpMethod = "PUT", notes = "修改指定用户基本信息,返回添加是否成功")
	public ResponseEntity<ServerResponse<Boolean>> updateSubscriber(
			@ApiParam(required = true, name = "subscriberId", value = "用户ID") @PathVariable("subscriberId") Long subscriberId,
			@ApiParam(required = true, name = "body") @RequestBody(required = true) SubscriberVo subscriberVo) {
		log.debug("用户{}修改指定用户{}基本信息", HmSessionFactory.currentUserId(), subscriberId);
		subscriberService.updateSubscriber(subscriberId, subscriberVo);
		return ControllerResult.success(true);
	}

	@RequestMapping(value = "/pwd/{subscriberId}", method = RequestMethod.PATCH)
	@ApiOperation(tags = SWWAGER_TAG, value = "修改指定用户登陆密码信息", httpMethod = "PATCH", notes = "修改指定用户登陆密码信息,返回添加是否成功")
	public ResponseEntity<ServerResponse<Boolean>> updateSubscriberPwd(
			@ApiParam(required = true, name = "subscriberId", value = "用户ID") @PathVariable("subscriberId") Long subscriberId,
			@ApiParam(required = true, name = "oldPwd", value = "旧密码") @RequestParam(required = true, name = "oldPwd") String oldPwd,
			@ApiParam(required = true, name = "newPwd", value = "新密码") @RequestParam(required = true, name = "newPwd") String newPwd) {
		log.debug("用户{}修改指定用户{}登陆密码信息", HmSessionFactory.currentUserId(), subscriberId);
		subscriberService.updateSubscriberPwd(subscriberId, oldPwd, newPwd);
		return ControllerResult.success(true);
	}

	@RequestMapping(value = "/force/pwd/{subscriberId}", method = RequestMethod.PATCH)
	@ApiOperation(tags = SWWAGER_TAG, value = "强制修改指定用户登陆密码信息", httpMethod = "PATCH", notes = "制修改指定用户登陆密码信息,返回添加是否成功")
	public ResponseEntity<ServerResponse<Boolean>> updateSubscriberPwd(
			@ApiParam(required = true, name = "subscriberId", value = "用户ID") @PathVariable("subscriberId") Long subscriberId,
			@ApiParam(required = true, name = "newPwd", value = "新密码") @RequestParam(required = true, name = "newPwd") String newPwd) {
		log.debug("用户{}制修改指定用户{}登陆密码信息", HmSessionFactory.currentUserId(), subscriberId);
		subscriberService.updateSubscriberPwd(subscriberId, null, newPwd);
		return ControllerResult.success(true);
	}

	@RequestMapping(method = RequestMethod.DELETE)
	@ApiOperation(tags = SWWAGER_TAG, value = "删除角色信息", httpMethod = "DELETE", notes = "删除角色信息,返回删除是否成功")
	public ResponseEntity<ServerResponse<Boolean>> deleteSubscriber(
			@ApiParam(required = true, name = "subscriberIds", value = "用户ID")  @RequestParam(required = true, name = "subscriberIds") List<Long> subscriberIds) {
		log.debug("用户{}删除角色信息", HmSessionFactory.currentUserId());
		subscriberService.deleteSubscriber(subscriberIds);
		return ControllerResult.success(true);
	}

	@RequestMapping(value = "/{pageNumber}/{pageSize}", method = RequestMethod.GET)
	@ApiOperation(tags = SWWAGER_TAG, value = "分页系统登录账户记录", httpMethod = "GET", notes = "分页系统登录账户记录")
	public ResponseEntity<ServerResponse<PageInfo<SubscriberVo>>> query(
			@ApiParam(required = true, name = "pageNumber", value = "分页号(第一页值为1,最小页码不能小于1)") @PathVariable("pageNumber") int pageNumber,
			@ApiParam(required = true, name = "pageSize", value = "分页大小") @PathVariable("pageSize") int pageSize,
			@ApiParam(required = false, name = "account", value = "电话号码或者邮箱或者登陆账号") @RequestParam(required = false, name = "account") String account)
			throws Exception {
		log.debug("用户{},查询分页系统登录账户记录", HmSessionFactory.currentUserId());
		return ControllerResult.success(subscriberService.query(new PageParam(pageNumber, pageSize), account));
	}

}
