package com.hm.base.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hm.base.auto.helper.HmSessionFactory;
import com.hm.base.auto.su.ControllerResult;
import com.hm.base.auto.su.R.Restful;
import com.hm.base.service.RoleService;
import com.hm.base.vo.RoleVo;
import com.hm.common.su.bean.PageInfo;
import com.hm.common.su.bean.PageInfo.PageParam;
import com.hm.common.su.bean.ServerResponse;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * @author shishun.wang
 * @date 下午3:40:02 2017年7月31日
 * @version 1.0
 * @describe
 */
@Slf4j
@ResponseBody
@RestController
@RequestMapping(Restful.API + "/role")
public class RoleApi {

	private static final String SWWAGER_TAG = "系统角色管理";

	@Autowired
	private RoleService roleService;

	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(tags = SWWAGER_TAG, value = "新增角色信息", httpMethod = "POST", notes = "新增用户信息,返回添加是否成功")
	public ResponseEntity<ServerResponse<Boolean>> addRole(
			@ApiParam(required = true, name = "body") @RequestBody(required = true) RoleVo roleVo) {
		log.debug("用户{}新增角色信息", HmSessionFactory.currentUserId());
		roleVo.setCreateUser(HmSessionFactory.currentUserId());
		roleService.addRole(roleVo);
		return ControllerResult.success(true);
	}

	@RequestMapping(value = "/{roleId}", method = RequestMethod.GET)
	@ApiOperation(tags = SWWAGER_TAG, value = "获取角色信息", httpMethod = "GET", notes = "获取角色信息,返回添加是否成功")
	public ResponseEntity<ServerResponse<RoleVo>> getRole(
			@ApiParam(required = true, name = "roleId", value = "角色ID") @PathVariable("roleId") Long roleId) {
		log.debug("用户{}获取角色信息", HmSessionFactory.currentUserId());
		return ControllerResult.success(roleService.getRole(roleId));
	}

	@RequestMapping(value = "/{roleId}", method = RequestMethod.PUT)
	@ApiOperation(tags = SWWAGER_TAG, value = "修改角色信息", httpMethod = "PUT", notes = "修改角色信息,返回添加是否成功")
	public ResponseEntity<ServerResponse<Boolean>> updateRole(
			@ApiParam(required = true, name = "roleId", value = "角色ID") @PathVariable("roleId") Long roleId,
			@ApiParam(required = true, name = "body") @RequestBody(required = true) RoleVo roleVo) {
		log.debug("用户{}修改角色信息", HmSessionFactory.currentUserId());
		roleVo.setId(roleId);
		roleService.updateRole(roleVo);
		return ControllerResult.success(true);
	}

	@RequestMapping(method = RequestMethod.DELETE)
	@ApiOperation(tags = SWWAGER_TAG, value = "删除角色信息", httpMethod = "DELETE", notes = "删除角色信息,返回删除是否成功")
	public ResponseEntity<ServerResponse<Boolean>> deleteRole(
			@ApiParam(required = true, name = "roleIds", value = "角色ID") @RequestParam(required = true, name = "roleIds") List<Long> roleIds) {
		log.debug("用户{}删除角色信息", HmSessionFactory.currentUserId());
		roleService.deleteRole(roleIds);
		return ControllerResult.success(true);
	}

	@RequestMapping(value = "/{pageNumber}/{pageSize}", method = RequestMethod.GET)
	@ApiOperation(tags = SWWAGER_TAG, value = "分页系统角色记录", httpMethod = "GET", notes = "分页系统角色记录")
	public ResponseEntity<ServerResponse<PageInfo<RoleVo>>> query(
			@ApiParam(required = true, name = "pageNumber", value = "分页号(第一页值为1,最小页码不能小于1)") @PathVariable("pageNumber") int pageNumber,
			@ApiParam(required = true, name = "pageSize", value = "分页大小") @PathVariable("pageSize") int pageSize,
			@ApiParam(required = false, name = "name", value = "名称") @RequestParam(required = false, name = "name") String name)
			throws Exception {
		log.debug("用户{},查询分页系统角色记录", HmSessionFactory.currentUserId());
		return ControllerResult.success(roleService.query(new PageParam(pageNumber, pageSize), name));
	}
}
