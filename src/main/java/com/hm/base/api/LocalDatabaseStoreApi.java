package com.hm.base.api;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hm.base.auto.SystemEnvConfigProperties;
import com.hm.base.auto.helper.HmSessionFactory;
import com.hm.base.auto.su.ControllerResult;
import com.hm.base.auto.su.R.Restful;
import com.hm.base.service.LocalDatabaseStoreService;
import com.hm.base.vo.LocalDatabaseStoreVo;
import com.hm.common.annotation.WebClientRequestIntercept;
import com.hm.common.su.bean.ServerResponse;
import com.hm.common.util.FileUtil;
import com.hm.common.util.ZipUtil;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * @author shishun.wang
 * @date 下午3:49:13 2017年8月25日
 * @version 1.0
 * @describe 本地数据库文件存储
 */
@Slf4j
@ResponseBody
@RestController
@RequestMapping(Restful.API + "/ldbstore")
public class LocalDatabaseStoreApi {

	private static final String SWWAGER_TAG = "系统数据管理";

	@Autowired
	private LocalDatabaseStoreService localDatabaseStoreService;

	@Autowired
	private SystemEnvConfigProperties systemEnvConfigProperties;

	@RequestMapping(value = "/load/files", method = RequestMethod.GET)
	@ApiOperation(tags = SWWAGER_TAG, value = "加载本地数据库文件存储列表", httpMethod = "GET", notes = "加载本地数据库文件存储列表")
	public ResponseEntity<ServerResponse<List<LocalDatabaseStoreVo>>> query() {
		log.debug("用户{},加载本地数据库文件存储列表", HmSessionFactory.currentUserId());
		return ControllerResult.success(localDatabaseStoreService.query());
	}

	@WebClientRequestIntercept(auth = false)
	@RequestMapping(value = "/{fileName}", method = RequestMethod.GET)
	@ApiOperation(tags = SWWAGER_TAG, value = "下载本地数据库文件", httpMethod = "PUT", notes = "下载本地数据库文件")
	public void downloadDatabaseStoreFile(
			@ApiParam(required = true, name = "fileName", value = "文件名称") @PathVariable("fileName") String fileName,
			HttpServletResponse response) {
		try {
			String path = systemEnvConfigProperties.getDatabaseBackUp().getBackUpFilePath().endsWith("/")
					? systemEnvConfigProperties.getDatabaseBackUp().getBackUpFilePath().substring(0,
							systemEnvConfigProperties.getDatabaseBackUp().getBackUpFilePath().length() - 1)
					: systemEnvConfigProperties.getDatabaseBackUp().getBackUpFilePath();

			File file = new File(path + "/" + fileName);
			if (!file.exists()) {
				log.warn("{},文件不存在.下载文件失败", fileName);
				return;
			}

			ArrayList<File> files = new ArrayList<File>();
			files.add(file);

			ZipUtil.compressFilesZip(files, path + "/" + fileName + ".zip",
					systemEnvConfigProperties.getZipFilePassword());

			FileUtil.download(new File(path + "/" + fileName + ".zip"), fileName + ".zip", response);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}
}
