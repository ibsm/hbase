package com.hm.base.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hm.base.auto.helper.HmSessionFactory;
import com.hm.base.auto.su.ControllerResult;
import com.hm.base.auto.su.R.Restful;
import com.hm.base.def.SystemConfigDictTypeEnum;
import com.hm.base.service.SystemConfigDictService;
import com.hm.base.vo.SystemConfigDictTypeEnumVo;
import com.hm.base.vo.SystemConfigDictVo;
import com.hm.common.su.bean.PageInfo;
import com.hm.common.su.bean.PageInfo.PageParam;
import com.hm.common.su.bean.ServerResponse;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * @author shishun.wang
 * @date 上午11:05:32 2017年6月14日
 * @version 1.0
 * @describe 系统配置管理
 */
@Slf4j
@ResponseBody
@RestController
@RequestMapping(Restful.API + "/syscnf")
public class SystemConfigDictApi {

	private static final String SWWAGER_TAG = "系统配置管理";

	@Autowired
	private SystemConfigDictService systemConfigDictService;

	@RequestMapping(value = "/dict/types", method = RequestMethod.PATCH)
	@ApiOperation(tags = SWWAGER_TAG, value = "加载系统数据字典所有分类", httpMethod = "PATCH", notes = "系统数据字典所有分类数据")
	public ResponseEntity<ServerResponse<List<SystemConfigDictTypeEnumVo>>> loadSystemConfigDictTypes() {
		log.debug("用户{},加载系统数据字典所有分类", HmSessionFactory.currentUserId());
		return ControllerResult.success(systemConfigDictService.loadSystemConfigDictTypes());
	}

	@RequestMapping(value = "/dict/{code}", method = RequestMethod.GET)
	@ApiOperation(tags = SWWAGER_TAG, value = "加载系统数据字典", httpMethod = "GET", notes = "根据数据字典CODE,查找相关满足条件的数据字典")
	public ResponseEntity<ServerResponse<SystemConfigDictVo>> loadSystemConfigDictByDictCode(
			@ApiParam(required = true, name = "code", value = "字典CODE") @PathVariable("code") String code) {
		log.debug("用户{},加载系统数据字典", HmSessionFactory.currentUserId());
		return ControllerResult.success(systemConfigDictService.loadSystemConfigDictByDictCode(code));
	}

	@RequestMapping(value = "/dicts/{dictType}", method = RequestMethod.GET)
	@ApiOperation(tags = SWWAGER_TAG, value = "加载系统数据字典列表", httpMethod = "GET", notes = "根据数据字典大类type,查找相关满足条件的数据字典,并倒序输出")
	public ResponseEntity<ServerResponse<List<SystemConfigDictVo>>> loadSystemConfigDictByDictType(
			@ApiParam(required = true, name = "dictType", value = "字典分类") @PathVariable("dictType") SystemConfigDictTypeEnum dictType) {
		log.debug("用户{},加载系统数据字典列表", HmSessionFactory.currentUserId());
		return ControllerResult.success(systemConfigDictService.loadSystemConfigDictByDictType(dictType));
	}

	@RequestMapping(value = "/dicts/{pageNumber}/{pageSize}", method = RequestMethod.GET)
	@ApiOperation(tags = SWWAGER_TAG, value = "分页系统数据字典列表", httpMethod = "GET", notes = "分页系统数据字典列表")
	public ResponseEntity<ServerResponse<PageInfo<SystemConfigDictVo>>> query(
			@ApiParam(required = true, name = "pageNumber", value = "分页号(第一页值为1,最小页码不能小于1)") @PathVariable("pageNumber") int pageNumber,
			@ApiParam(required = true, name = "pageSize", value = "分页大小") @PathVariable("pageSize") int pageSize,
			@ApiParam(required = false, name = "dictType", value = "字典分类") @RequestParam(required = false, name = "dictType") SystemConfigDictTypeEnum dictType,
			@ApiParam(required = false, name = "code", value = "字典CODE") @RequestParam(required = false, name = "code") String code)
			throws Exception {
		log.debug("用户{},查询系统数据字典列表", HmSessionFactory.currentUserId());
		return ControllerResult
				.success(systemConfigDictService.query(new PageParam(pageNumber, pageSize), dictType, code));
	}
}
