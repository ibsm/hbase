package com.hm.base.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hm.base.auto.helper.HmSessionFactory;
import com.hm.base.auto.su.ControllerResult;
import com.hm.base.auto.su.R.Restful;
import com.hm.base.service.ReqLogService;
import com.hm.base.vo.ReqLogVo;
import com.hm.common.su.bean.PageInfo;
import com.hm.common.su.bean.PageInfo.PageParam;
import com.hm.common.su.bean.ServerResponse;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * @author shishun.wang
 * @date 下午2:39:07 2017年8月21日
 * @version 1.0
 * @describe
 */
@Slf4j
@ResponseBody
@RestController
@RequestMapping(Restful.API + "/reqlog")
public class ReqLogApi {

	private static final String SWWAGER_TAG = "请求日志管理";

	@Autowired
	private ReqLogService reqLogService;

	@RequestMapping(value = "/{pageNumber}/{pageSize}", method = RequestMethod.GET)
	@ApiOperation(tags = SWWAGER_TAG, value = "分页文件管理记录", httpMethod = "GET", notes = "分页文件管理记录")
	public ResponseEntity<ServerResponse<PageInfo<ReqLogVo>>> query(
			@ApiParam(required = true, name = "pageNumber", value = "分页号(第一页值为1,最小页码不能小于1)") @PathVariable("pageNumber") int pageNumber,
			@ApiParam(required = true, name = "pageSize", value = "分页大小") @PathVariable("pageSize") int pageSize,
			@ApiParam(required = false, name = "reqName", value = "请求名称") @RequestParam(required = false, name = "reqName") String reqName,
			@ApiParam(required = false, name = "startCreateTime", value = "创建时间开始日期") @RequestParam(required = false, name = "startCreateTime") Long startCreateTime,
			@ApiParam(required = false, name = "endCreateTime", value = "创建时间结束日期") @RequestParam(required = false, name = "endCreateTime") Long endCreateTime)
			throws Exception {
		log.debug("用户{},查询分页文件管理记录", HmSessionFactory.currentUserId());
		return ControllerResult.success(reqLogService.query(new PageParam(pageNumber, pageSize), reqName,
				HmSessionFactory.currentUserId(), startCreateTime, endCreateTime));
	}

}
