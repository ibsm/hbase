package com.hm.base.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hm.base.auto.helper.HmSessionFactory;
import com.hm.base.auto.su.ControllerResult;
import com.hm.base.auto.su.R.Restful;
import com.hm.base.service.AsynDownloadService;
import com.hm.base.vo.AsynDownloadVo;
import com.hm.common.su.bean.PageInfo;
import com.hm.common.su.bean.PageInfo.PageParam;
import com.hm.common.su.bean.ServerResponse;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * @author shishun.wang
 * @date 下午3:15:34 2017年8月21日
 * @version 1.0
 * @describe
 */
@Slf4j
@ResponseBody
@RestController
@RequestMapping(Restful.API + "/asyndownload")
public class AsynDownloadApi {

	private static final String SWWAGER_TAG = "异步文件管理";

	@Autowired
	private AsynDownloadService asynDownloadService;

	@RequestMapping(value = "/{pageNumber}/{pageSize}", method = RequestMethod.GET)
	@ApiOperation(tags = SWWAGER_TAG, value = "分页文件管理记录", httpMethod = "GET", notes = "分页文件管理记录")
	public ResponseEntity<ServerResponse<PageInfo<AsynDownloadVo>>> query(
			@ApiParam(required = true, name = "pageNumber", value = "分页号(第一页值为1,最小页码不能小于1)") @PathVariable("pageNumber") int pageNumber,
			@ApiParam(required = true, name = "pageSize", value = "分页大小") @PathVariable("pageSize") int pageSize,
			@ApiParam(required = false, name = "name", value = "文件名称") @RequestParam(required = false, name = "name") String name)
			throws Exception {
		log.debug("用户{},查询分页文件管理记录", HmSessionFactory.currentUserId());
		return ControllerResult.success(asynDownloadService.query(new PageParam(pageNumber, pageSize), name));
	}
}
