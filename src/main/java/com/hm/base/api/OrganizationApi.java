package com.hm.base.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hm.base.auto.SystemEnvConfigProperties;
import com.hm.base.auto.helper.HmSessionFactory;
import com.hm.base.auto.su.ControllerResult;
import com.hm.base.auto.su.R.Restful;
import com.hm.base.service.OrganizationService;
import com.hm.base.vo.OrganizationVo;
import com.hm.common.exception.ServiceException;
import com.hm.common.su.bean.PageInfo;
import com.hm.common.su.bean.PageInfo.PageParam;
import com.hm.common.su.bean.ServerResponse;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * @author shishun.wang
 * @date 下午10:03:32 2017年8月17日
 * @version 1.0
 * @describe
 */
@Slf4j
@ResponseBody
@RestController
@RequestMapping(Restful.API + "/orga")
public class OrganizationApi {

	private static final String SWWAGER_TAG = "组织机构管理";

	@Autowired
	private OrganizationService organizationService;

	@Autowired
	private SystemEnvConfigProperties systemEnvConfigProperties;

	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(tags = SWWAGER_TAG, value = "新增组织机构信息", httpMethod = "POST", notes = "新增用户信息,返回添加是否成功")
	public ResponseEntity<ServerResponse<Boolean>> addOrganization(
			@ApiParam(required = true, name = "body") @RequestBody(required = true) OrganizationVo organizationVo) {
		log.debug("用户{}新增组织机构信息", HmSessionFactory.currentUserId());
		if (!systemEnvConfigProperties.isOpenOrganization()) {
			throw ServiceException.warn("当前系统基础服务不支持该接口使用");
		}
		organizationVo.setCreateUser(HmSessionFactory.currentUserId());
		organizationService.addOrganization(organizationVo);
		return ControllerResult.success(true);
	}

	@RequestMapping(value = "/{orgaId}", method = RequestMethod.GET)
	@ApiOperation(tags = SWWAGER_TAG, value = "获取组织机构信息", httpMethod = "PUT", notes = "获取组织机构信息,返回添加是否成功")
	public ResponseEntity<ServerResponse<OrganizationVo>> getOrganization(
			@ApiParam(required = true, name = "orgaId", value = "组织机构ID") @PathVariable("orgaId") Long orgaId) {
		log.debug("用户{}获取组织机构信息", HmSessionFactory.currentUserId());
		if (!systemEnvConfigProperties.isOpenOrganization()) {
			throw ServiceException.warn("当前系统基础服务不支持该接口使用");
		}
		return ControllerResult.success(organizationService.getOrganization(orgaId));
	}

	@RequestMapping(value = "/{orgaId}", method = RequestMethod.PUT)
	@ApiOperation(tags = SWWAGER_TAG, value = "修改组织机构信息", httpMethod = "PUT", notes = "修改组织机构信息,返回添加是否成功")
	public ResponseEntity<ServerResponse<Boolean>> updateOrganization(
			@ApiParam(required = true, name = "orgaId", value = "组织机构ID") @PathVariable("orgaId") Long orgaId,
			@ApiParam(required = true, name = "body") @RequestBody(required = true) OrganizationVo organizationVo) {
		log.debug("用户{}修改组织机构信息", HmSessionFactory.currentUserId());
		if (!systemEnvConfigProperties.isOpenOrganization()) {
			throw ServiceException.warn("当前系统基础服务不支持该接口使用");
		}
		organizationVo.setId(orgaId);
		organizationService.updateOrganization(orgaId, organizationVo);
		return ControllerResult.success(true);
	}

	@RequestMapping(method = RequestMethod.DELETE)
	@ApiOperation(tags = SWWAGER_TAG, value = "删除组织机构信息", httpMethod = "DELETE", notes = "删除组织机构信息,返回删除是否成功")
	public ResponseEntity<ServerResponse<Boolean>> deleteOrganization(
			@ApiParam(required = true, name = "orgaIds", value = "组织机构ID") @RequestParam(required = true, name = "orgaIds") List<Long> orgaIds) {
		log.debug("用户{}删除组织机构信息", HmSessionFactory.currentUserId());
		if (!systemEnvConfigProperties.isOpenOrganization()) {
			throw ServiceException.warn("当前系统基础服务不支持该接口使用");
		}
		organizationService.deleteOrganization(orgaIds);
		return ControllerResult.success(true);
	}

	@RequestMapping(value = "/{pageNumber}/{pageSize}", method = RequestMethod.GET)
	@ApiOperation(tags = SWWAGER_TAG, value = "分页系统组织机构信息记录", httpMethod = "GET", notes = "分页系统组织机构信息记录")
	public ResponseEntity<ServerResponse<PageInfo<OrganizationVo>>> query(
			@ApiParam(required = true, name = "pageNumber", value = "分页号(第一页值为1,最小页码不能小于1)") @PathVariable("pageNumber") int pageNumber,
			@ApiParam(required = true, name = "pageSize", value = "分页大小") @PathVariable("pageSize") int pageSize,
			@ApiParam(required = false, name = "parentId", value = "上级组织机构信息Id") @RequestParam(required = false, name = "parentId") Long parentId,
			@ApiParam(required = false, name = "name", value = "名称") @RequestParam(required = false, name = "name") String name)
			throws Exception {
		log.debug("用户{},查询分页系统组织机构信息记录", HmSessionFactory.currentUserId());
		if (!systemEnvConfigProperties.isOpenOrganization()) {
			throw ServiceException.warn("当前系统基础服务不支持该接口使用");
		}
		return ControllerResult.success(organizationService.query(new PageParam(pageNumber, pageSize), parentId, name));
	}

}
