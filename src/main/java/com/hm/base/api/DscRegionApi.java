package com.hm.base.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hm.base.auto.helper.HmSessionFactory;
import com.hm.base.auto.su.ControllerResult;
import com.hm.base.auto.su.R.Restful;
import com.hm.base.def.RegionTypeEnum;
import com.hm.base.service.DscRegionService;
import com.hm.base.vo.DscRegionVo;
import com.hm.common.exception.ErrorCode;
import com.hm.common.exception.ServiceException;
import com.hm.common.su.bean.ServerResponse;
import com.hm.common.util.CommonUtil;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * @author shishun.wang
 * @date 上午11:27:52 2017年8月7日
 * @version 1.0
 * @describe
 */
@Slf4j
@ResponseBody
@RestController
@RequestMapping(Restful.API + "/dregion")
public class DscRegionApi {

	private static final String SWWAGER_TAG = "国家区域管理";

	@Autowired
	private DscRegionService dscRegionService;

	@RequestMapping(value = "load/dregions/{regionType}/{parentId}", method = RequestMethod.GET)
	@ApiOperation(tags = SWWAGER_TAG, value = "加载国家区域数据", httpMethod = "GET", notes = "分页系统角色记录")
	public ResponseEntity<ServerResponse<List<DscRegionVo>>> loadDscRegions(
			@ApiParam(required = true, name = "parentId", value = "上级区域ID，第一级默认为0") @PathVariable("parentId") Long parentId,
			@ApiParam(required = true, name = "regionType", value = "区域类型") @PathVariable("regionType") RegionTypeEnum regionType) {
		log.debug("用户{}加载国家区域数据", HmSessionFactory.currentUserId());
		if (CommonUtil.isAnyEmpty(parentId, regionType)) {
			throw ServiceException.warning(ErrorCode.REQUIRED_PARAMETERS_MISSING);
		}
		return ControllerResult.success(dscRegionService.loadDscRegions(parentId, regionType));
	}
}
