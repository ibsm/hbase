package com.hm.base.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hm.base.auto.SystemEnvConfigProperties;
import com.hm.base.auto.helper.HmSessionFactory;
import com.hm.base.auto.su.ControllerResult;
import com.hm.base.auto.su.R.Restful;
import com.hm.base.service.AuthorizationService;
import com.hm.base.vo.LoginSubscriberVo;
import com.hm.base.vo.MenuVo;
import com.hm.base.vo.OrganizationVo;
import com.hm.base.vo.RoleMenuVo;
import com.hm.base.vo.RoleVo;
import com.hm.common.annotation.WebClientRequestIntercept;
import com.hm.common.exception.ErrorCode;
import com.hm.common.exception.ServiceException;
import com.hm.common.su.bean.ServerResponse;
import com.hm.common.util.CommonUtil;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * @author shishun.wang
 * @date 下午4:52:19 2017年6月2日
 * @version 1.0
 * @describe 授权信息管理
 */
@Slf4j
@ResponseBody
@RestController
@RequestMapping(Restful.API + "/auth")
public class AuthorizationApi {

	private static final String SWWAGER_TAG = "授权信息管理";

	@Autowired
	private AuthorizationService authorizationService;

	@Autowired
	private SystemEnvConfigProperties systemEnvConfigProperties;

	@WebClientRequestIntercept(auth = false)
	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(tags = SWWAGER_TAG, value = "用户登陆", httpMethod = "POST", notes = "后端用户登录接口")
	public ResponseEntity<ServerResponse<LoginSubscriberVo>> addSubscriber(
			@ApiParam(required = true, name = "username", value = "账户名称") @RequestParam("username") String username,
			@ApiParam(required = true, name = "password", value = "账户密码") @RequestParam("password") String password) {
		log.debug("用户{},试图登陆系统", username);
		return ControllerResult.success(authorizationService.login(username, password));
	}

	@RequestMapping(path = "/logout", method = RequestMethod.POST)
	@ApiOperation(tags = SWWAGER_TAG, value = "退出系统", httpMethod = "POST", notes = "退出后端系统")
	public ResponseEntity<ServerResponse<Boolean>> logout(
			@ApiParam(required = true, name = "username", value = "账户名称") @RequestParam("username") String username) {
		log.debug("用户{},退出系统", username);

		if (CommonUtil.isEmpty(username)) {
			log.error("退出系统失败,账户名称不能为空");
			throw ServiceException.warning(ErrorCode.DATA_NOT_NULL, "账户名称不能为空");
		}

		authorizationService.logout(HmSessionFactory.currentUserId(), username);
		return ControllerResult.success(true);
	}

	@RequestMapping(path = "/user/roles", method = RequestMethod.PATCH)
	@ApiOperation(tags = SWWAGER_TAG, value = "加载用户角色列表(备注：适用于用户->角色->菜单模式调用)", httpMethod = "PATCH", notes = "加载用户角色列表(备注：适用于用户->角色->菜单模式调用)")
	public ResponseEntity<ServerResponse<List<RoleVo>>> loadUserRoles(
			@ApiParam(required = false, name = "subscriberId", value = "账户ID") @RequestParam(required = false, name = "subscriberId") Long subscriberId) {
		log.debug("加载用户{}角色列表", HmSessionFactory.currentUserId());

		if (systemEnvConfigProperties.isOpenOrganization()) {
			throw ServiceException.warn("当前系统基础服务不支持该接口使用");
		}
		if (CommonUtil.isNotEmpty(subscriberId)) {
			return ControllerResult.success(authorizationService.loadUserRoles(subscriberId));
		}
		return ControllerResult.success(authorizationService.loadUserRoles(HmSessionFactory.currentUserId()));
	}

	@RequestMapping(path = "/user/menus", method = RequestMethod.PATCH)
	@ApiOperation(tags = SWWAGER_TAG, value = "加载用户菜单列表(备注：适用于用户->角色->菜单模式调用)", httpMethod = "PATCH", notes = "加载用户菜单列表(备注：适用于用户->角色->菜单模式调用)")
	public ResponseEntity<ServerResponse<List<MenuVo>>> loadUserMenus() {
		log.debug("加载用户{}菜单列表", HmSessionFactory.currentUserId());

		if (systemEnvConfigProperties.isOpenOrganization()) {
			throw ServiceException.warn("当前系统基础服务不支持该接口使用");
		}
		return ControllerResult.success(authorizationService.loadUserMenus(HmSessionFactory.currentUserId()));
	}

	@RequestMapping(path = "/user/organizations", method = RequestMethod.PATCH)
	@ApiOperation(tags = SWWAGER_TAG, value = "加载用户组织机构列表(备注：适用于用户->组织机构->角色->菜单模式调用)", httpMethod = "PATCH", notes = "加载用户组织机构列表(备注：适用于用户->组织机构->角色->菜单模式调用)")
	public ResponseEntity<ServerResponse<List<OrganizationVo>>> loadUserOrganizations(
			@ApiParam(required = false, name = "subscriberId", value = "账户ID") @RequestParam(required = false, name = "subscriberId") Long subscriberId) {
		log.debug("加载用户{}组织机构列表", HmSessionFactory.currentUserId());

		if (!systemEnvConfigProperties.isOpenOrganization()) {
			throw ServiceException.warn("当前系统基础服务不支持该接口使用");
		}

		if (CommonUtil.isEmpty(subscriberId)) {
			return ControllerResult.success(authorizationService.loadUserOrganizations(subscriberId));
		}
		return ControllerResult.success(authorizationService.loadUserOrganizations(HmSessionFactory.currentUserId()));
	}

	@RequestMapping(path = "/user/orgin/roles", method = RequestMethod.PATCH)
	@ApiOperation(tags = SWWAGER_TAG, value = "加载用户角色列表(备注：适用于用户->组织机构->角色->菜单模式调用)", httpMethod = "PATCH", notes = "加载用户角色列表(备注：适用于用户->组织机构->角色->菜单模式调用)")
	public ResponseEntity<ServerResponse<List<RoleVo>>> loadUserOrganizationRoles() {
		log.debug("加载用户{}角色列表", HmSessionFactory.currentUserId());

		if (!systemEnvConfigProperties.isOpenOrganization()) {
			throw ServiceException.warn("当前系统基础服务不支持该接口使用");
		}
		return ControllerResult
				.success(authorizationService.loadUserOrganizationRoles(HmSessionFactory.currentUserId()));
	}

	@RequestMapping(path = "/user/orgin/menus", method = RequestMethod.PATCH)
	@ApiOperation(tags = SWWAGER_TAG, value = "加载用户菜单列表(备注：适用于用户->组织机构->角色->菜单模式调用)", httpMethod = "PATCH", notes = "加载用户菜单列表(备注：适用于用户->组织机构->角色->菜单模式调用)")
	public ResponseEntity<ServerResponse<List<MenuVo>>> loadUserOrganizationMenus() {
		log.debug("加载用户{}菜单列表", HmSessionFactory.currentUserId());

		if (!systemEnvConfigProperties.isOpenOrganization()) {
			throw ServiceException.warn("当前系统基础服务不支持该接口使用");
		}
		return ControllerResult
				.success(authorizationService.loadUserOrganizationMenus(HmSessionFactory.currentUserId()));
	}

	@RequestMapping(path = "/setting/user/organizations", method = RequestMethod.PATCH)
	@ApiOperation(tags = SWWAGER_TAG, value = "设置用户组织机构(备注：适用于用户->组织机构->角色->菜单模式调用)", httpMethod = "PATCH", notes = "设置用户组织机构(备注：适用于用户->组织机构->角色->菜单模式调用)")
	public ResponseEntity<ServerResponse<Boolean>> settingUserOrganizations(
			@ApiParam(required = false, name = "subscriberId", value = "账户ID") @RequestParam(required = false, name = "subscriberId") Long subscriberId,
			@ApiParam(required = false, name = "organizations", value = "组织机构ID") @RequestParam(required = false, name = "organizations") List<Long> organizations) {
		log.debug("用户{},设置用户组织机构", HmSessionFactory.currentUserId());
		if (!systemEnvConfigProperties.isOpenOrganization()) {
			throw ServiceException.warn("当前系统基础服务不支持该接口使用");
		}

		if (CommonUtil.isEmpty(subscriberId)) {
			throw ServiceException.warning(ErrorCode.PARAMETERS_MISSING);
		}
		authorizationService.settingUserOrganizations(subscriberId, organizations);
		return ControllerResult.success(true);
	}

	@RequestMapping(path = "/setting/user/orgin/roles", method = RequestMethod.PATCH)
	@ApiOperation(tags = SWWAGER_TAG, value = "设置用户角色(备注：适用于用户->组织机构->角色->菜单模式调用)", httpMethod = "PATCH", notes = "设置用户角色(备注：适用于用户->组织机构->角色->菜单模式调用)")
	public ResponseEntity<ServerResponse<Boolean>> settingUserOrganizationRoles(
			@ApiParam(required = false, name = "organizationId", value = "组织机构ID") @RequestParam(required = false, name = "organizationId") Long organizationId,
			@ApiParam(required = false, name = "roles", value = "角色ID") @RequestParam(required = false, name = "roles") List<Long> roles) {
		log.debug("用户{},设置用户角色", HmSessionFactory.currentUserId());

		if (!systemEnvConfigProperties.isOpenOrganization()) {
			throw ServiceException.warn("当前系统基础服务不支持该接口使用");
		}
		authorizationService.settingUserOrganizationRoles(organizationId, roles);
		return ControllerResult.success(true);
	}

	@RequestMapping(path = "/setting/user/roles", method = RequestMethod.PATCH)
	@ApiOperation(tags = SWWAGER_TAG, value = "设置用户角色(备注：适用于用户->角色->菜单模式调用)", httpMethod = "PATCH", notes = "设置用户角色(备注：适用于用户->角色->菜单模式调用)")
	public ResponseEntity<ServerResponse<Boolean>> settingUserRoles(
			@ApiParam(required = false, name = "subscriberId", value = "账户ID") @RequestParam(required = false, name = "subscriberId") Long subscriberId,
			@ApiParam(required = false, name = "roles", value = "角色ID") @RequestParam(required = false, name = "roles") List<Long> roles) {
		log.debug("用户{},设置用户角色", HmSessionFactory.currentUserId());

		if (systemEnvConfigProperties.isOpenOrganization()) {
			throw ServiceException.warn("当前系统基础服务不支持该接口使用");
		}

		if (CommonUtil.isEmpty(subscriberId)) {
			throw ServiceException.warning(ErrorCode.PARAMETERS_MISSING);
		}
		authorizationService.settingUserRoles(subscriberId, roles);
		return ControllerResult.success(true);
	}

	@RequestMapping(path = "/setting/user/role/menus", method = RequestMethod.PATCH)
	@ApiOperation(tags = SWWAGER_TAG, value = "设置用户角色所属菜单", httpMethod = "PATCH", notes = "设置用户角色所属菜单")
	public ResponseEntity<ServerResponse<Boolean>> settingRoleMenus(
			@ApiParam(required = false, name = "roleId", value = "角色ID") @RequestParam(required = false, name = "roleId") Long roleId,
			@ApiParam(required = false, name = "menus", value = "菜单ID") @RequestParam(required = false, name = "menus") List<Long> menus) {
		log.debug("用户{},设置用户角色", HmSessionFactory.currentUserId());
		authorizationService.settingRoleMenus(roleId, menus);
		return ControllerResult.success(true);
	}

	@RequestMapping(path = "/user/role/menus", method = RequestMethod.PATCH)
	@ApiOperation(tags = SWWAGER_TAG, value = "加载用户角色所属菜单", httpMethod = "PATCH", notes = "加载用户角色所属菜单")
	public ResponseEntity<ServerResponse<List<RoleMenuVo>>> loadRoleMenus(
			@ApiParam(required = false, name = "roleId", value = "角色ID") @RequestParam(required = false, name = "roleId") Long roleId) {
		log.debug("用户{},加载用户角色所属菜单", HmSessionFactory.currentUserId());
		return ControllerResult.success(authorizationService.loadRoleMenus(roleId));
	}
}
