package com.hm.base.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hm.base.auto.helper.HmSessionFactory;
import com.hm.base.auto.helper.ScheduleTriggerHandler;
import com.hm.base.auto.su.ControllerResult;
import com.hm.base.auto.su.R.Restful;
import com.hm.base.def.ScheduleJobStatusEnum;
import com.hm.base.service.ScheduleJobService;
import com.hm.base.vo.ScheduleJobVo;
import com.hm.common.su.bean.PageInfo;
import com.hm.common.su.bean.PageInfo.PageParam;
import com.hm.common.su.bean.ServerResponse;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

/**
 * @author shishun.wang
 * @date 下午3:40:11 2017年8月24日
 * @version 1.0
 * @describe
 */
@Slf4j
@ResponseBody
@RestController
@RequestMapping(Restful.API + "/scjob")
public class ScheduleJobApi {

	private static final String SWWAGER_TAG = "动态任务管理";

	@Autowired
	private ScheduleJobService scheduleJobService;

	@Autowired
	private ScheduleTriggerHandler scheduleTriggerHandler;

	@RequestMapping(method = RequestMethod.POST)
	@ApiOperation(tags = SWWAGER_TAG, value = "新增动态定时任务信息", httpMethod = "POST", notes = "新增用户信息,返回添加是否成功")
	public ResponseEntity<ServerResponse<Boolean>> addScheduleJob(
			@ApiParam(required = true, name = "body") @RequestBody(required = true) ScheduleJobVo scheduleJobVo) {
		log.debug("用户{}新增动态定时任务信息", HmSessionFactory.currentUserId());
		scheduleJobVo.setCreateUser(HmSessionFactory.currentUserId());
		scheduleJobService.addScheduleJob(scheduleJobVo);
		return ControllerResult.success(true);
	}

	@RequestMapping(value = "/{scheduleJobId}", method = RequestMethod.GET)
	@ApiOperation(tags = SWWAGER_TAG, value = "获取动态定时任务信息", httpMethod = "GET", notes = "获取动态定时任务信息,返回添加是否成功")
	public ResponseEntity<ServerResponse<ScheduleJobVo>> getScheduleJob(
			@ApiParam(required = true, name = "scheduleJobId", value = "动态定时任务ID") @PathVariable("scheduleJobId") Long scheduleJobId) {
		log.debug("用户{}获取动态定时任务信息", HmSessionFactory.currentUserId());
		return ControllerResult.success(scheduleJobService.getScheduleJob(scheduleJobId));
	}

	@RequestMapping(value = "/{scheduleJobId}", method = RequestMethod.PUT)
	@ApiOperation(tags = SWWAGER_TAG, value = "修改动态定时任务信息", httpMethod = "PUT", notes = "修改动态定时任务信息,返回添加是否成功")
	public ResponseEntity<ServerResponse<Boolean>> updateScheduleJob(
			@ApiParam(required = true, name = "scheduleJobId", value = "动态定时任务ID") @PathVariable("scheduleJobId") Long scheduleJobId,
			@ApiParam(required = true, name = "body") @RequestBody(required = true) ScheduleJobVo scheduleJobVo) {
		log.debug("用户{}修改动态定时任务信息", HmSessionFactory.currentUserId());
		scheduleJobVo.setId(scheduleJobId);
		scheduleJobService.updateScheduleJob(scheduleJobId, scheduleJobVo);
		return ControllerResult.success(true);
	}

	@RequestMapping(method = RequestMethod.DELETE)
	@ApiOperation(tags = SWWAGER_TAG, value = "删除动态定时任务信息", httpMethod = "DELETE", notes = "删除动态定时任务信息,返回删除是否成功")
	public ResponseEntity<ServerResponse<Boolean>> deleteScheduleJob(
			@ApiParam(required = true, name = "scheduleJobIds", value = "动态定时任务ID") @RequestParam(required = true, name = "scheduleJobIds") List<Long> scheduleJobIds) {
		log.debug("用户{}删除动态定时任务信息", HmSessionFactory.currentUserId());
		scheduleJobService.deleteScheduleJob(scheduleJobIds);
		return ControllerResult.success(true);
	}

	@RequestMapping(value = "/{scheduleJobId}/{scheduleJobStatus}", method = RequestMethod.PATCH)
	@ApiOperation(tags = SWWAGER_TAG, value = "修改动态定时任务定时状态", httpMethod = "PATCH", notes = "修改动态定时任务定时状态,返回修改定时任务状态是否成功")
	public ResponseEntity<ServerResponse<Boolean>> updateScheduleJobStatus(
			@ApiParam(required = true, name = "scheduleJobId", value = "动态定时任务ID") @PathVariable("scheduleJobId") Long scheduleJobId,
			@ApiParam(required = true, name = "scheduleJobStatus", value = "任务状态") @PathVariable("scheduleJobStatus") ScheduleJobStatusEnum scheduleJobStatus) {
		log.debug("用户{}修改动态定时任务定时状态信息", HmSessionFactory.currentUserId());
		scheduleJobService.updateScheduleJobStatus(scheduleJobId, scheduleJobStatus);
		return ControllerResult.success(true);
	}

	@RequestMapping(value = "/{pageNumber}/{pageSize}", method = RequestMethod.GET)
	@ApiOperation(tags = SWWAGER_TAG, value = "分页系统动态定时任务记录", httpMethod = "GET", notes = "分页系统动态定时任务记录")
	public ResponseEntity<ServerResponse<PageInfo<ScheduleJobVo>>> query(
			@ApiParam(required = true, name = "pageNumber", value = "分页号(第一页值为1,最小页码不能小于1)") @PathVariable("pageNumber") int pageNumber,
			@ApiParam(required = true, name = "pageSize", value = "分页大小") @PathVariable("pageSize") int pageSize,
			@ApiParam(required = false, name = "jobGroup", value = "任务分组") @RequestParam(required = false, name = "jobGroup") String jobGroup,
			@ApiParam(required = false, name = "scheduleJobStatus", value = "任务状态") @RequestParam(required = false, name = "scheduleJobStatus") ScheduleJobStatusEnum scheduleJobStatus)
			throws Exception {
		log.debug("用户{},查询分页系统动态定时任务记录", HmSessionFactory.currentUserId());
		return ControllerResult
				.success(scheduleJobService.query(new PageParam(pageNumber, pageSize), jobGroup, scheduleJobStatus));
	}

	@RequestMapping(value = "/refresh", method = RequestMethod.PATCH)
	@ApiOperation(tags = SWWAGER_TAG, value = "刷新所有动态定时任务定时状态", httpMethod = "PATCH", notes = "刷新所有动态定时任务定时状态,返回刷新所有动态定时任务定时状态是否成功")
	public ResponseEntity<ServerResponse<Boolean>> refreshScheduleJobs() {
		log.debug("用户{}刷新所有动态定时任务定时状态", HmSessionFactory.currentUserId());
		scheduleTriggerHandler.refreshScheduleJobs();
		return ControllerResult.success(true);
	}

	@RequestMapping(value = "/refresh/{scheduleJobId}", method = RequestMethod.PATCH)
	@ApiOperation(tags = SWWAGER_TAG, value = "刷新动态定时任务定时状态", httpMethod = "PATCH", notes = "刷新动态定时任务定时状态,返回刷新动态定时任务定时状态是否成功")
	public ResponseEntity<ServerResponse<Boolean>> refreshScheduleJob(
			@ApiParam(required = true, name = "scheduleJobId", value = "动态定时任务ID") @PathVariable("scheduleJobId") Long scheduleJobId) {
		log.debug("用户{}刷新动态定时任务定时状态", HmSessionFactory.currentUserId());
		scheduleTriggerHandler.refreshScheduleJob(scheduleJobId);
		return ControllerResult.success(true);
	}

	@RequestMapping(value = "/realtime/{scheduleJobId}", method = RequestMethod.PATCH)
	@ApiOperation(tags = SWWAGER_TAG, value = "执行及时定时任务定时调度", httpMethod = "PATCH", notes = "执行及时定时任务定时调度,返回执行及时定时任务定时调度是否成功")
	public ResponseEntity<ServerResponse<Boolean>> realTimeScheduleJob(
			@ApiParam(required = true, name = "scheduleJobId", value = "动态定时任务ID") @PathVariable("scheduleJobId") Long scheduleJobId) {
		log.debug("用户{}执行及时定时任务定时调度", HmSessionFactory.currentUserId());
		scheduleJobService.realTimeScheduleJob(scheduleJobId);
		return ControllerResult.success(true);
	}

}
