package com.hm.base.domain;

import java.util.Date;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author shishun.wang
 * @date 下午2:54:33 2017年8月21日
 * @version 1.0
 * @describe 文件异步下载
 */
@Table(name = "hm_asyn_download")
public class AsynDownload {

	/**
	 * 编号ID
	 */
	@Id
	private Long id;

	/**
	 * 文件名
	 */
	private String fileName;

	/**
	 * 文件描述
	 */
	private String fileDesc;

	/**
	 * 下载地址
	 */
	private String downloadUri;

	/**
	 * 文件数据上报状态
	 */
	private String fileStatus;

	/**
	 * 上传文件hashcode
	 */
	private String hashCode;

	/**
	 * 创建人
	 */
	private Long createUser;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 数据状态
	 */
	private String status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName == null ? null : fileName.trim();
	}

	public String getFileDesc() {
		return fileDesc;
	}

	public void setFileDesc(String fileDesc) {
		this.fileDesc = fileDesc == null ? null : fileDesc.trim();
	}

	public String getDownloadUri() {
		return downloadUri;
	}

	public void setDownloadUri(String downloadUri) {
		this.downloadUri = downloadUri == null ? null : downloadUri.trim();
	}

	public String getFileStatus() {
		return fileStatus;
	}

	public void setFileStatus(String fileStatus) {
		this.fileStatus = fileStatus == null ? null : fileStatus.trim();
	}

	public Long getCreateUser() {
		return createUser;
	}

	public void setCreateUser(Long createUser) {
		this.createUser = createUser;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status == null ? null : status.trim();
	}

	public String getHashCode() {
		return hashCode;
	}

	public void setHashCode(String hashCode) {
		this.hashCode = hashCode == null ? null : hashCode.trim();
	}

}