package com.hm.base.domain;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author shishun.wang
 * @date 上午11:01:07 2017年8月7日
 * @version 1.0
 * @describe
 */
@Table(name = "hm_dsc_region")
public class DscRegion {
	
	/**
	 * 
	 */
	@Id
	private Long id;

	/**
	 * 
	 */
	private Long parentId;

	/**
	 * 
	 */
	private String regionName;

	/**
	 * 
	 */
	private Integer regionType;

	/**
	 * 
	 */
	private Long agencyId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName == null ? null : regionName.trim();
	}

	public Integer getRegionType() {
		return regionType;
	}

	public void setRegionType(Integer regionType) {
		this.regionType = regionType;
	}

	public Long getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(Long agencyId) {
		this.agencyId = agencyId;
	}
}