package com.hm.base.domain;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author shishun.wang
 * @date 下午6:43:11 2017年6月15日
 * @version 1.0
 * @describe
 */
@Table(name = "hm_organization_role")
public class OrganizationRole {
	/**
	 * ID
	 */
	@Id
	private Long id;

	/**
	 * 组织ID
	 */
	private Long organizationId;

	/**
	 * 角色ID
	 */
	private Long roleId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
}