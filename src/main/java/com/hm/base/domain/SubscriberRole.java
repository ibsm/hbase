package com.hm.base.domain;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author shishun.wang
 * @date 下午2:33:56 2017年6月5日
 * @version 1.0
 * @describe 
 */
@Table(name = "hm_subscriber_role")
public class SubscriberRole {
    /**
     * 编号ID
     */
	@Id
    private Long id;

    /**
     * 用户ID
     */
    private Long subscriberId;

    /**
     * 角色ID
     */
    private Long roleId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSubscriberId() {
        return subscriberId;
    }

    public void setSubscriberId(Long subscriberId) {
        this.subscriberId = subscriberId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
}