package com.hm.base.domain;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author shishun.wang
 * @date 下午6:42:52 2017年6月15日
 * @version 1.0
 * @describe
 */
@Table(name = "hm_subscriber_organization")
public class SubscriberOrganization {
	/**
	 * 编号ID
	 */
	@Id
	private Long id;

	/**
	 * 用户ID
	 */
	private Long userId;

	/**
	 * 组织ID
	 */
	private Long organizationId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}
}