package com.hm.base.domain;

import java.util.Date;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author shishun.wang
 * @date 下午2:34:43 2017年6月5日
 * @version 1.0
 * @describe 
 */
@Table(name = "hm_menu")
public class Menu {
    /**
     * 编号ID
     */
	@Id
    private Long id;

    /**
     * 菜单名称
     */
    private String name;

    /**
     * 备注描述
     */
    private String note;

    /**
     * 请求地址
     */
    private String uri;

    /**
     * 图标
     */
    private String ico;

    /**
     * 上级id
     */
    private Long parentId;

    /**
     * 排列顺序
     */
    private Long sort;

    /**
     * 创建人
     */
    private Long createUser;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 使用状态
     */
    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri == null ? null : uri.trim();
    }

    public String getIco() {
        return ico;
    }

    public void setIco(String ico) {
        this.ico = ico == null ? null : ico.trim();
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getSort() {
        return sort;
    }

    public void setSort(Long sort) {
        this.sort = sort;
    }

    public Long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }
}