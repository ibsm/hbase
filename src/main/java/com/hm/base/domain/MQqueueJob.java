package com.hm.base.domain;

import java.util.Date;

import javax.persistence.Table;

/**
 * @author shishun.wang
 * @date 下午6:20:21 2017年9月5日
 * @version 1.0
 * @describe 
 */
@Table(name = "hm_mq_queue_job")
public class MQqueueJob {
    /**
     * 编号ID
     */
    private Long id;

    /**
     * 消息名称
     */
    private String name;

    /**
     * 消息分组
     */
    private String group;

    /**
     * 消息模式
     */
    private String model;

    /**
     * 任务标签
     */
    private String jobTag;

    /**
     * 备注
     */
    private String note;

    /**
     * 任务状态
     */
    private String jobStatus;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 创建人
     */
    private Long createUser;

    /**
     * 数据状态
     */
    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group == null ? null : group.trim();
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model == null ? null : model.trim();
    }

    public String getJobTag() {
        return jobTag;
    }

    public void setJobTag(String jobTag) {
        this.jobTag = jobTag == null ? null : jobTag.trim();
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus == null ? null : jobStatus.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }
}