package com.hm.base.domain;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author shishun.wang
 * @date 下午2:34:48 2017年6月5日
 * @version 1.0
 * @describe 
 */
@Table(name = "hm_role_menu")
public class RoleMenu {
    /**
     * 编号ID
     */
	@Id
    private Long id;

    /**
     * 菜单ID
     */
    private Long menuId;

    /**
     * 角色ID
     */
    private Long roleId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
}