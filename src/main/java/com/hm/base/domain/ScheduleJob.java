package com.hm.base.domain;

import java.util.Date;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author shishun.wang
 * @date 上午11:48:17 2017年8月24日
 * @version 1.0
 * @describe 定时任务时间配置器
 */
@Table(name = "hm_schedule_job")
public class ScheduleJob {

	/**
	 * 编号ID
	 */
	@Id
	private Long id;

	/**
	 * 任务名称
	 */
	private String jobName;

	/**
	 * 任务分组
	 */
	private String jobGroup;

	/**
	 * 任务描述
	 */
	private String note;

	/**
	 * 任务时间表达式
	 */
	private String cron;

	/**
	 * 任务状态，使用、暂停、禁止
	 */
	private String jobStatus;

	/**
	 * 及时推送
	 */
	private String realTime;

	/**
	 * 及时推送处理者
	 */
	private String realTimeHandler;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 创建人
	 */
	private Long createUser;

	/**
	 * 数据状态
	 */
	private String status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName == null ? null : jobName.trim();
	}

	public String getJobGroup() {
		return jobGroup;
	}

	public void setJobGroup(String jobGroup) {
		this.jobGroup = jobGroup == null ? null : jobGroup.trim();
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note == null ? null : note.trim();
	}

	public String getCron() {
		return cron;
	}

	public void setCron(String cron) {
		this.cron = cron == null ? null : cron.trim();
	}

	public String getJobStatus() {
		return jobStatus;
	}

	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus == null ? null : jobStatus.trim();
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Long getCreateUser() {
		return createUser;
	}

	public void setCreateUser(Long createUser) {
		this.createUser = createUser;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status == null ? null : status.trim();
	}

	public String getRealTime() {
		return realTime;
	}

	public void setRealTime(String realTime) {
		this.realTime = realTime == null ? null : realTime.trim();
	}

	public String getRealTimeHandler() {
		return realTimeHandler;
	}

	public void setRealTimeHandler(String realTimeHandler) {
		this.realTimeHandler = realTimeHandler == null ? null : realTimeHandler.trim();
	}

}