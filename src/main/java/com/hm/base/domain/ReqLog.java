package com.hm.base.domain;

import java.util.Date;

import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author shishun.wang
 * @date 下午2:38:21 2017年8月21日
 * @version 1.0
 * @describe 请求uri日志
 */
@Table(name = "hm_req_log")
public class ReqLog {
    /**
     * 编号ID
     */
	@Id
    private Long id;

    /**
     * 请求名称
     */
    private String reqName;
    
    /**
     * 请求数据来源（数据模块，数据服务名称）
     */
    private String reqSource;

    /**
     * 请求uri
     */
    private String reqUri;

    /**
     * 请求参数
     */
    private String params;

    /**
     * 请求描述
     */
    private String note;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 创建人
     */
    private Long createUser;

    /**
     * 使用状态
     */
    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReqName() {
        return reqName;
    }

    public void setReqName(String reqName) {
        this.reqName = reqName == null ? null : reqName.trim();
    }

    public String getReqUri() {
        return reqUri;
    }

    public void setReqUri(String reqUri) {
        this.reqUri = reqUri == null ? null : reqUri.trim();
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params == null ? null : params.trim();
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note == null ? null : note.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

	public String getReqSource() {
		return reqSource;
	}

	public void setReqSource(String reqSource) {
		this.reqSource = reqSource == null ? null : reqSource.trim();
	}
    
    
}