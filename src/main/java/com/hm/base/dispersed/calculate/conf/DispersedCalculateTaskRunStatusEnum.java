package com.hm.base.dispersed.calculate.conf;

import com.hm.common.util.CommonUtil;

/**
 * @author shishun.wang
 * @date 上午10:11:50 2017年9月18日
 * @version 1.0
 * @describe
 */
public enum DispersedCalculateTaskRunStatusEnum {

	/**
	 * 运行中
	 */
	RUNNING("RUNNING", "运行中"),

	/**
	 * 暂停
	 */
	PAUSE("PAUSE", "暂停"),

	/**
	 * 停止
	 */
	STOP("STOP", "停止"),

	/**
	 * 已完成
	 */
	FINISHED("FINISHED", "已完成");

	private String status;

	private String desc;

	private DispersedCalculateTaskRunStatusEnum(String status, String desc) {
		this.status = status;
		this.desc = desc;
	}

	public String desc() {
		return this.desc;
	}

	public String status() {
		return this.status;
	}

	public static DispersedCalculateTaskRunStatusEnum trance(String status) {
		if (CommonUtil.isEmpty(status)) {
			return null;
		}

		for (DispersedCalculateTaskRunStatusEnum enumTmp : DispersedCalculateTaskRunStatusEnum.values()) {
			if (enumTmp.status.equals(status)) {
				return enumTmp;
			}
		}
		return null;
	}
}
