package com.hm.base.dispersed.calculate.su;

/**
 * @author shishun.wang
 * @date 上午11:04:44 2017年9月18日
 * @version 1.0
 * @describe 
 */
public interface DispersedCalculateHandler {

	/**
	 * @param jobRunTag 分页数据页码
	 */
	public void handler(Long jobRunTag);
}
