package com.hm.base.dispersed.calculate.conf;

/**
 * @author shishun.wang
 * @date 上午9:39:16 2017年9月18日
 * @version 1.0
 * @describe
 */
public interface DispersedCalculate {

	public String VERSION = "_V1.0";

	public String PREFIX = "HM_DISPERSED_CALCULATE" + VERSION;

	/**
	 * 标注任务状态，运行中，暂停、停止  HM_DISPERSED_CALCULATE_V1.0:TASK_RUN_STATUS:业务名称   状态值
	 */
	public String TASK_RUN_STATUS = PREFIX + ":TASK_RUN_STATUS:";

	/**
	 * 任务执行标识，当前运行第几块数据 HM_DISPERSED_CALCULATE_V1.0:JOB_RUN_TAG:业务名称     自增长块id
	 */
	public String JOB_RUN_TAG = PREFIX + ":JOB_RUN_TAG:";

	/**
	 * 任务执行标识，执行状态,数据处理中，数据处理成功，数据处理失败,key描述HM_DISPERSED_CALCULATE_V1.0:JOB_RUN_TAG_STATUS:业务名称_自增长块id     状态值
	 */
	public String JOB_RUN_TAG_STATUS = PREFIX + ":JOB_RUN_TAG_STATUS:";
}
