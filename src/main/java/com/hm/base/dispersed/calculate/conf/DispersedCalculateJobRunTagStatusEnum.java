package com.hm.base.dispersed.calculate.conf;

import com.hm.common.util.CommonUtil;

/**
 * @author shishun.wang
 * @date 上午10:49:25 2017年9月18日
 * @version 1.0
 * @describe
 */
public enum DispersedCalculateJobRunTagStatusEnum {

	/**
	 * 处理中
	 */
	PROCESSING("PROCESSING", "处理中"),

	/**
	 * 处理成功
	 */
	SUCCESSFUL("SUCCESSFUL", "成功"),

	/**
	 * 处理失败
	 */
	FAILURE("FAILURE", "失败");

	private String status;

	private String desc;

	private DispersedCalculateJobRunTagStatusEnum(String status, String desc) {
		this.status = status;
		this.desc = desc;
	}

	public String desc() {
		return this.desc;
	}

	public String status() {
		return this.status;
	}

	public static DispersedCalculateJobRunTagStatusEnum trance(String status) {
		if (CommonUtil.isEmpty(status)) {
			return null;
		}

		for (DispersedCalculateJobRunTagStatusEnum enumTmp : DispersedCalculateJobRunTagStatusEnum.values()) {
			if (enumTmp.status.equals(status)) {
				return enumTmp;
			}
		}
		return null;
	}
}
