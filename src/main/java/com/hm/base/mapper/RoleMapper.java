package com.hm.base.mapper;

import java.util.List;

import com.hm.base.auto.su.HmMapper;
import com.hm.base.domain.Role;

/**
 * @author shishun.wang
 * @date 上午11:30:18 2017年6月5日
 * @version 1.0
 * @describe
 */
public interface RoleMapper extends HmMapper<Role> {
	
	public List<Role> loadUserRoles(Long subscriberId);
	
	public List<Role> loadUserOrganizationRoles(Long subscriberId);
	
	public List<Role> query(Role role);
	
	public Role checkRoleName(String name);
	
	public void deleteRole(Long roleId);
	
}