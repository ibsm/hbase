package com.hm.base.mapper;

import java.util.List;
import java.util.Map;

import com.hm.base.auto.su.HmMapper;
import com.hm.base.domain.Organization;

/**
 * @author shishun.wang
 * @date 下午6:48:28 2017年6月15日
 * @version 1.0
 * @describe 
 */
public interface OrganizationMapper  extends HmMapper<Organization> {
	
	public List<Organization> loadUserOrganizations(Map<String,Object> param);

	public void deleteOrganization(Long orgaId);
	
	public List<Organization> query(Organization organization);
}