package com.hm.base.mapper;

import java.util.List;
import java.util.Map;

import com.hm.base.auto.su.HmMapper;
import com.hm.base.domain.Menu;

/**
 * @author shishun.wang
 * @date 上午11:45:52 2017年6月5日
 * @version 1.0
 * @describe
 */
public interface MenuMapper extends HmMapper<Menu> {

	public List<Menu> loadUserMenus(Map<String,Object> param);
	
	public List<Menu> loadUserOrganizationMenus(Map<String,Object> param);
	
	public List<Menu> query(Menu menu);
}