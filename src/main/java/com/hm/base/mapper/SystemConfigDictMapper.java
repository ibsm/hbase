package com.hm.base.mapper;

import java.util.List;

import com.hm.base.auto.su.HmMapper;
import com.hm.base.domain.SystemConfigDict;

/**
 * @author shishun.wang
 * @date 上午10:50:37 2017年6月14日
 * @version 1.0
 * @describe
 */
public interface SystemConfigDictMapper extends HmMapper<SystemConfigDict> {

	public List<SystemConfigDict> loadSystemConfigDictByDictType(String dictType);

	public SystemConfigDict loadSystemConfigDictByDictCode(String code);

	public List<SystemConfigDict> query(SystemConfigDict dict);

}