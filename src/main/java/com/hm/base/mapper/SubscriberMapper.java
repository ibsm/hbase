package com.hm.base.mapper;

import java.util.List;

import com.hm.base.auto.su.HmMapper;
import com.hm.base.domain.Subscriber;

/**
 * @author shishun.wang
 * @date 上午11:30:16 2017年6月5日
 * @version 1.0
 * @describe 
 */
public interface SubscriberMapper extends HmMapper<Subscriber> {

	Subscriber login(Subscriber subscriber);

	Subscriber checkAccount(String account);

	Subscriber checkUsername(String userName);

	Subscriber checkEmail(String email);

	Subscriber checkPhone(String phone);

	List<Subscriber> query(Subscriber subscriber);
}