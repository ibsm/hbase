package com.hm.base.mapper;

import com.hm.base.auto.su.HmMapper;
import com.hm.base.domain.ServerInstanceCfg;

public interface ServerInstanceCfgMapper extends HmMapper<ServerInstanceCfg>{
	
}