package com.hm.base.mapper;

import com.hm.base.auto.su.HmMapper;
import com.hm.base.domain.SubscriberOrganization;

/**
 * @author shishun.wang
 * @date 下午6:44:06 2017年6月15日
 * @version 1.0
 * @describe 
 */
public interface SubscriberOrganizationMapper extends HmMapper<SubscriberOrganization> {
}