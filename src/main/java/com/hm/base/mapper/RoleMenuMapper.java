package com.hm.base.mapper;

import com.hm.base.auto.su.HmMapper;
import com.hm.base.domain.RoleMenu;

/**
 * @author shishun.wang
 * @date 上午11:45:47 2017年6月5日
 * @version 1.0
 * @describe 
 */
public interface RoleMenuMapper extends HmMapper<RoleMenu> {
	
}