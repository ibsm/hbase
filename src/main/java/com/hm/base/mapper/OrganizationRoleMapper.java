package com.hm.base.mapper;

import com.hm.base.auto.su.HmMapper;
import com.hm.base.domain.OrganizationRole;

/**
 * @author shishun.wang
 * @date 下午6:43:54 2017年6月15日
 * @version 1.0
 * @describe 
 */
public interface OrganizationRoleMapper extends HmMapper<OrganizationRole> {
	
}