package com.hm.base.mapper;

import java.util.List;
import java.util.Map;

import com.hm.base.auto.su.HmMapper;
import com.hm.base.domain.ReqLog;

public interface ReqLogMapper extends HmMapper<ReqLog> {

	public List<ReqLog> query(Map<String,Object> params);
}