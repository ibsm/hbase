package com.hm.base.mapper;

import java.util.List;

import com.hm.base.auto.su.HmMapper;
import com.hm.base.domain.AsynDownload;

public interface AsynDownloadMapper extends HmMapper<AsynDownload> {

	public List<AsynDownload> query(AsynDownload asynDownload);
}