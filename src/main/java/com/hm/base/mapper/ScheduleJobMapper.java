package com.hm.base.mapper;

import java.util.List;

import com.hm.base.auto.su.HmMapper;
import com.hm.base.domain.ScheduleJob;

public interface ScheduleJobMapper extends HmMapper<ScheduleJob>{

	public List<ScheduleJob> query(ScheduleJob scheduleJob);
}