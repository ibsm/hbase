package com.hm.base.mapper;

import com.hm.base.auto.su.HmMapper;
import com.hm.base.domain.SubscriberRole;

/**
 * @author shishun.wang
 * @date 上午11:36:22 2017年6月5日
 * @version 1.0
 * @describe 
 */
public interface SubscriberRoleMapper extends HmMapper<SubscriberRole> {
}