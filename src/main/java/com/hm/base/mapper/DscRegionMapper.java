package com.hm.base.mapper;

import com.hm.base.auto.su.HmMapper;
import com.hm.base.domain.DscRegion;

/**
 * @author shishun.wang
 * @date 上午11:23:02 2017年8月7日
 * @version 1.0
 * @describe
 */
public interface DscRegionMapper extends HmMapper<DscRegion> {

}