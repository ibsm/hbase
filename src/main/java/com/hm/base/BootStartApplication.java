package com.hm.base;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.socket.config.annotation.EnableWebSocket;

/**
 * @author shishun.wang
 * @date 下午2:28:06 2017年5月26日
 * @version 1.0
 * @describe
 */
@EnableWebSocket
@EnableScheduling
@SpringBootApplication
@ComponentScan(basePackages = { "com.hm" })
public class BootStartApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootStartApplication.class, args);
	}
}
