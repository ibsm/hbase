package com.hm.base.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.hm.base.auto.SystemEnvConfigProperties;
import com.hm.base.auto.su.BeanUtil;
import com.hm.base.domain.Role;
import com.hm.base.mapper.RoleMapper;
import com.hm.base.vo.RoleVo;
import com.hm.common.def.DataStatusDef;
import com.hm.common.exception.ErrorCode;
import com.hm.common.exception.ServiceException;
import com.hm.common.su.bean.PageInfo;
import com.hm.common.su.bean.PageInfo.PageParam;
import com.hm.common.util.CommonUtil;
import com.hm.common.util.DateUtil;
import com.hm.common.util.StringUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * @author shishun.wang
 * @date 上午11:58:53 2017年6月5日
 * @version 1.0
 * @describe
 */
@Slf4j
@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleMapper roleMapper;

	@Autowired
	private SystemEnvConfigProperties systemEnvConfigProperties;

	public List<RoleVo> loadAllRoles() {
		List<RoleVo> vos = new ArrayList<RoleVo>();
		roleMapper.selectAll().forEach(role -> {
			RoleVo vo = new RoleVo();
			BeanUtil.copyProperties(role, vo);
			vo.setCreateTime(DateUtil.data2long(role.getCreateTime()));

			vos.add(vo);
		});
		return vos;
	}

	@Override
	public PageInfo<RoleVo> query(PageParam pageParam, String name) {
		Page<Role> page = PageHelper.startPage(pageParam.getPage(), pageParam.getSize(), true);

		Role role = new Role();
		if (StringUtil.isNotBlank(name)) {
			role.setName(name);
		}

		roleMapper.query(role);
		List<RoleVo> vos = new ArrayList<RoleVo>();
		page.getResult().forEach(item -> {
			RoleVo vo = new RoleVo();
			BeanUtil.copyProperties(item, vo);
			vo.setCreateTime(DateUtil.data2long(item.getCreateTime()));

			vos.add(vo);
		});

		return new PageInfo<RoleVo>(vos, page.getPages(), page.getTotal());
	}

	@Override
	public void addRole(RoleVo vo) {
		if (CommonUtil.isEmpty(vo.getName())) {
			throw ServiceException.warning(ErrorCode.REQUIRED_PARAMETERS_MISSING);
		}
		Role query = new Role();
		{
			query.setName(vo.getName());
		}
		if (CommonUtil.isNotEmpty(roleMapper.selectOne(query))) {
			throw ServiceException.warning(ErrorCode.DATA_EXISTING);
		}

		Role role = new Role();
		{
			BeanUtil.copyProperties(vo, role);
			role.setId(null);
			role.setCreateTime(new Date());
			role.setStatus(DataStatusDef.ENABLE.name());
		}

		roleMapper.insert(role);
	}

	@Override
	public void updateRole(RoleVo vo) {
		if (CommonUtil.isAnyEmpty(vo.getId(), vo.getName())) {
			throw ServiceException.warning(ErrorCode.REQUIRED_PARAMETERS_MISSING);
		}
		Role query = new Role();
		{
			query.setName(vo.getName());
		}
		Role queryRole = roleMapper.selectOne(query);
		if (CommonUtil.isNotEmpty(queryRole) && queryRole.getId() != vo.getId()) {
			throw ServiceException.warning(ErrorCode.DATA_EXISTING);
		}

		Role role = roleMapper.selectByPrimaryKey(vo.getId());
		if (CommonUtil.isEmpty(role)) {
			throw ServiceException.warning(ErrorCode.DATA_NOT_FOUND);
		}
		role.setName(vo.getName());
		role.setNote(vo.getNote());
		roleMapper.updateByPrimaryKey(role);
	}

	@Override
	public void deleteRole(List<Long> roleIds) {
		if (CommonUtil.isEmpty(roleIds)) {
			throw ServiceException.warning(ErrorCode.REQUIRED_PARAMETERS_MISSING);
		}

		roleIds.parallelStream().forEach(roleId -> {
			Role role = roleMapper.selectByPrimaryKey(roleId);
			if (CommonUtil.isEmpty(role)) {
				return;
			}

			if (systemEnvConfigProperties.isProtectInitUser() && 1 == roleId) {
				log.error("{},用户系统配置了保护,无权删除数据", roleId);
				return;
			}

			role.setStatus(DataStatusDef.DISABLE.name());
			roleMapper.updateByPrimaryKey(role);
		});

	}

	@Override
	public RoleVo getRole(Long roleId) {
		if (CommonUtil.isEmpty(roleId)) {
			throw ServiceException.warning(ErrorCode.REQUIRED_PARAMETERS_MISSING);
		}
		Role role = roleMapper.selectByPrimaryKey(roleId);
		if (CommonUtil.isEmpty(role)) {
			throw ServiceException.warning(ErrorCode.DATA_NOT_FOUND);
		}

		RoleVo vo = new RoleVo();
		BeanUtil.copyProperties(role, vo);

		return vo;
	}

}
