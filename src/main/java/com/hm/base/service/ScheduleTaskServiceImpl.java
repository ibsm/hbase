package com.hm.base.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hm.base.def.ScheduleMqPushTaskEnum;
import com.hm.base.job.su.ScheduleRemoteMqPushTaskSupport;
import com.hm.base.job.su.helper.ScheduleRemoteMqMuchTaskHandler;
import com.hm.base.job.su.helper.ScheduleRemoteMqSingleTaskHandler;
import com.hm.base.vo.ScheduleTaskCallResultVo;
import com.hm.base.vo.ScheduleTaskVo;
import com.hm.common.exception.ServiceException;
import com.hm.common.util.CommonUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * @author shishun.wang
 * @date 上午11:23:05 2017年9月13日
 * @version 1.0
 * @describe
 */
@Slf4j
@Service
public class ScheduleTaskServiceImpl implements ScheduleTaskService {

	@Autowired
	private ScheduleRemoteMqMuchTaskHandler scheduleRemoteMqMuchTaskHandler;

	@Autowired
	private ScheduleRemoteMqSingleTaskHandler scheduleRemoteMqSingleTaskHandler;

	@Override
	public ScheduleTaskCallResultVo mqPushTask(ScheduleMqPushTaskEnum mqPushTaskEnum, ScheduleTaskVo scheduleTaskVo) {

		ScheduleRemoteMqPushTaskSupport pushTaskSupport = identifyScheduleMqPushTask(mqPushTaskEnum);
		if (CommonUtil.isEmpty(pushTaskSupport)) {
			log.error("无法识别的推送消息任务类型");
			throw ServiceException.warn("无法识别的推送消息任务类型");
		}

		return pushTaskSupport.pushTask(scheduleTaskVo);
	}

	@Override
	public ScheduleTaskCallResultVo promptlyPushMqTask(ScheduleMqPushTaskEnum mqPushTaskEnum, ScheduleTaskVo scheduleTaskVo) {
		return this.mqPushTask(mqPushTaskEnum, scheduleTaskVo);
	}

	private ScheduleRemoteMqPushTaskSupport identifyScheduleMqPushTask(ScheduleMqPushTaskEnum mqPushTaskEnum) {
		switch (mqPushTaskEnum) {
		case SINGLE_SERVER_INSTANCE:
			return scheduleRemoteMqSingleTaskHandler;
		case SUBSCRIBE_ALL_SERVER_INSTANCE:
			return scheduleRemoteMqMuchTaskHandler;
		default:
			break;
		}
		return null;
	}

}
