package com.hm.base.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.hm.base.auto.su.BeanUtil;
import com.hm.base.def.SystemConfigDictTypeEnum;
import com.hm.base.domain.SystemConfigDict;
import com.hm.base.mapper.SystemConfigDictMapper;
import com.hm.base.vo.SystemConfigDictTypeEnumVo;
import com.hm.base.vo.SystemConfigDictVo;
import com.hm.common.exception.ErrorCode;
import com.hm.common.exception.ServiceException;
import com.hm.common.su.bean.PageInfo;
import com.hm.common.su.bean.PageInfo.PageParam;
import com.hm.common.util.CommonUtil;
import com.hm.common.util.DateUtil;

/**
 * @author shishun.wang
 * @date 上午11:02:23 2017年6月14日
 * @version 1.0
 * @describe
 */
@Service
public class SystemConfigDictServiceImpl implements SystemConfigDictService {

	@Autowired
	private SystemConfigDictMapper systemConfigDictMapper;

	@Override
	public List<SystemConfigDictTypeEnumVo> loadSystemConfigDictTypes() {
		List<SystemConfigDictTypeEnumVo> vos = new ArrayList<SystemConfigDictTypeEnumVo>();
		for (SystemConfigDictTypeEnum configDictType : SystemConfigDictTypeEnum.values()) {
			SystemConfigDictTypeEnumVo vo = new SystemConfigDictTypeEnumVo();
			BeanUtil.copyProperties(configDictType, vo);
			vos.add(vo);
		}

		return vos;
	}

	@Override
	public List<SystemConfigDictVo> loadSystemConfigDictByDictType(SystemConfigDictTypeEnum dictType) {
		if (CommonUtil.isEmpty(dictType)) {
			throw ServiceException.warning(ErrorCode.PARAMETERS_MISSING);
		}

		List<SystemConfigDictVo> vos = new ArrayList<SystemConfigDictVo>();
		systemConfigDictMapper.loadSystemConfigDictByDictType(dictType.name()).forEach(dict -> {
			SystemConfigDictVo vo = new SystemConfigDictVo();
			BeanUtil.copyProperties(dict, vo);
			vos.add(vo);
		});
		return vos;
	}

	@Override
	public SystemConfigDictVo loadSystemConfigDictByDictCode(String code) {
		if (CommonUtil.isEmpty(code)) {
			throw ServiceException.warning(ErrorCode.PARAMETERS_MISSING);
		}

		SystemConfigDict systemConfigDict = systemConfigDictMapper.loadSystemConfigDictByDictCode(code);
		if (CommonUtil.isEmpty(systemConfigDict)) {
			throw ServiceException.warning(ErrorCode.DATA_NOT_FOUND, code);
		}

		SystemConfigDictVo vo = new SystemConfigDictVo();
		BeanUtil.copyProperties(systemConfigDict, vo);

		return vo;
	}

	@Override
	public PageInfo<SystemConfigDictVo> query(PageParam pageParam, SystemConfigDictTypeEnum dictType, String code) {
		Page<SystemConfigDict> page = PageHelper.startPage(pageParam.getPage(), pageParam.getSize(), true);
		SystemConfigDict dict = new SystemConfigDict();
		if (CommonUtil.isNotEmpty(dictType)) {
			dict.setType(dictType.getStatus());
		}
		if (CommonUtil.isNotEmpty(code)) {
			dict.setCode(code);
		}
		systemConfigDictMapper.query(dict);

		List<SystemConfigDictVo> vos = new ArrayList<SystemConfigDictVo>();
		page.getResult().forEach(item -> {
			SystemConfigDictVo vo = new SystemConfigDictVo();
			BeanUtil.copyProperties(item, vo);
			SystemConfigDictTypeEnum type = SystemConfigDictTypeEnum.trance(item.getType());
			vo.setType(CommonUtil.isNotEmpty(type) ? type.getDesc() : null);
			vo.setCreateTime(DateUtil.data2long(item.getCreateTime()));

			vos.add(vo);
		});

		return new PageInfo<SystemConfigDictVo>(vos, page.getPages(), page.getTotal());
	}

}
