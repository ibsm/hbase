package com.hm.base.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.hm.base.auto.su.BeanUtil;
import com.hm.base.domain.Organization;
import com.hm.base.mapper.OrganizationMapper;
import com.hm.base.vo.OrganizationVo;
import com.hm.common.def.DataStatusDef;
import com.hm.common.exception.ErrorCode;
import com.hm.common.exception.ServiceException;
import com.hm.common.su.bean.PageInfo;
import com.hm.common.su.bean.PageInfo.PageParam;
import com.hm.common.util.CommonUtil;
import com.hm.common.util.DateUtil;
import com.hm.common.util.StringUtil;

/**
 * @author shishun.wang
 * @date 下午10:01:01 2017年8月17日
 * @version 1.0
 * @describe
 */
@Service
public class OrganizationServiceImpl implements OrganizationService {

	@Autowired
	private OrganizationMapper organizationMapper;

	@Override
	public void addOrganization(OrganizationVo organization) {
		if (CommonUtil.isAnyEmpty(organization.getName(), organization.getCode())) {
			throw ServiceException.warning(ErrorCode.REQUIRED_PARAMETERS_MISSING);
		}

		Organization query = new Organization();
		{
			query.setName(organization.getName());
		}
		if (CommonUtil.isNotEmpty(organizationMapper.selectOne(query))) {
			throw ServiceException.warning(ErrorCode.DATA_EXISTING);
		}
		query.setName(null);
		query.setCode(organization.getCode());
		if (CommonUtil.isNotEmpty(organizationMapper.selectOne(query))) {
			throw ServiceException.warning(ErrorCode.DATA_EXISTING);
		}

		Organization orga = new Organization();
		BeanUtil.copyProperties(organization, orga);
		orga.setId(null);
		organizationMapper.insert(orga);
	}

	@Override
	public void updateOrganization(Long orgaId, OrganizationVo organization) {
		if (CommonUtil.isAnyEmpty(orgaId, organization.getName(), organization.getCode())) {
			throw ServiceException.warning(ErrorCode.REQUIRED_PARAMETERS_MISSING);
		}

		Organization query = new Organization();
		{
			query.setName(organization.getName());
		}

		Organization result = organizationMapper.selectOne(query);
		if (CommonUtil.isNotEmpty(result) && result.getId() != orgaId) {
			throw ServiceException.warning(ErrorCode.DATA_EXISTING);
		}

		query.setName(null);
		query.setCode(organization.getCode());
		result = organizationMapper.selectOne(query);
		if (CommonUtil.isNotEmpty(result) && result.getId() != orgaId) {
			throw ServiceException.warning(ErrorCode.DATA_EXISTING);
		}

		Organization orga = new Organization();
		BeanUtil.copyProperties(organization, orga);
		orga.setId(orgaId);
		organizationMapper.updateByPrimaryKey(orga);

	}

	@Override
	public void deleteOrganization(List<Long> orgaIds) {
		if (CommonUtil.isEmpty(orgaIds)) {
			throw ServiceException.warning(ErrorCode.REQUIRED_PARAMETERS_MISSING);
		}

		orgaIds.parallelStream().forEach(orgaId ->{
			
			Organization organization = organizationMapper.selectByPrimaryKey(orgaId);
			if (CommonUtil.isEmpty(organization)) {
				return;
			}
			
			organization.setStatus(DataStatusDef.DISABLE.name());
			organizationMapper.updateByPrimaryKeySelective(organization);
		});
		
	}

	@Override
	public OrganizationVo getOrganization(Long orgaId) {
		if (CommonUtil.isEmpty(orgaId)) {
			throw ServiceException.warning(ErrorCode.REQUIRED_PARAMETERS_MISSING);
		}

		Organization organization = organizationMapper.selectByPrimaryKey(orgaId);
		if (CommonUtil.isEmpty(organization)) {
			throw ServiceException.warning(ErrorCode.DATA_NOT_FOUND);
		}
		OrganizationVo vo = new OrganizationVo();
		BeanUtil.copyProperties(organization, vo);
		return vo;
	}

	@Override
	public PageInfo<OrganizationVo> query(PageParam pageParam, Long parentId, String name) {
		Page<Organization> page = PageHelper.startPage(pageParam.getPage(), pageParam.getSize(), true);

		Organization orga = new Organization();
		if (StringUtil.isNotBlank(name)) {
			orga.setName(name);
		}
		if (CommonUtil.isNotEmpty(parentId)) {
			orga.setParentId(parentId);
		}

		organizationMapper.query(orga);
		List<OrganizationVo> vos = new ArrayList<OrganizationVo>();
		page.getResult().forEach(item -> {
			OrganizationVo vo = new OrganizationVo();
			BeanUtil.copyProperties(item, vo);
			vo.setCreateTime(DateUtil.data2long(item.getCreateTime()));
			if (CommonUtil.isNotEmpty(vo.getParentId())) {
				Organization parentOrganization = organizationMapper.selectByPrimaryKey(vo.getParentId());
				if (CommonUtil.isNotEmpty(parentOrganization)) {
					vo.setParentName(parentOrganization.getName());
				}
			}

			vos.add(vo);
		});

		return new PageInfo<OrganizationVo>(vos, page.getPages(), page.getTotal());
	}

}
