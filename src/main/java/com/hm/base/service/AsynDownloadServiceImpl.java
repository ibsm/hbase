package com.hm.base.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.hm.base.auto.su.BeanUtil;
import com.hm.base.domain.AsynDownload;
import com.hm.base.mapper.AsynDownloadMapper;
import com.hm.base.vo.AsynDownloadVo;
import com.hm.common.def.DataStatusDef;
import com.hm.common.exception.ErrorCode;
import com.hm.common.exception.ServiceException;
import com.hm.common.su.bean.PageInfo;
import com.hm.common.su.bean.PageInfo.PageParam;
import com.hm.common.util.CommonUtil;
import com.hm.common.util.DateUtil;
import com.hm.common.util.StringUtil;

/**
 * @author shishun.wang
 * @date 下午3:16:43 2017年8月21日
 * @version 1.0
 * @describe
 */
@Service
public class AsynDownloadServiceImpl implements AsynDownloadService {

	@Autowired
	private AsynDownloadMapper asynDownloadMapper;

	@Override
	public PageInfo<AsynDownloadVo> query(PageParam pageParam, String name) {
		Page<AsynDownload> page = PageHelper.startPage(pageParam.getPage(), pageParam.getSize(), true);

		AsynDownload asynDownload = new AsynDownload();
		if (StringUtil.isNotBlank(name)) {
			asynDownload.setFileName(name);
		}

		asynDownloadMapper.query(asynDownload);
		List<AsynDownloadVo> vos = new ArrayList<AsynDownloadVo>();
		page.getResult().forEach(item -> {
			AsynDownloadVo vo = new AsynDownloadVo();
			BeanUtil.copyProperties(item, vo);
			vo.setCreateTime(DateUtil.data2long(item.getCreateTime()));

			vos.add(vo);
		});

		return new PageInfo<AsynDownloadVo>(vos, page.getPages(), page.getTotal());
	}

	@Override
	public void addAsynDownloadTask(AsynDownloadVo asynDownloadVo) {
		if (CommonUtil.isEmpty(asynDownloadVo.getFileName())) {
			throw ServiceException.warning(ErrorCode.REQUIRED_PARAMETERS_MISSING);
		}

		AsynDownload asynDownload = new AsynDownload();
		{
			BeanUtil.copyProperties(asynDownloadVo, asynDownload);
			asynDownload.setId(null);
			asynDownload.setCreateTime(new Date());
			asynDownload.setStatus(DataStatusDef.ENABLE.name());
		}

		asynDownloadMapper.insert(asynDownload);
	}

	@Override
	public void updateAsynDownloadTask(AsynDownloadVo vo) {
		if (CommonUtil.isAnyEmpty(vo.getId(), vo.getAsynDownloadUploadStatu())) {
			throw ServiceException.warning(ErrorCode.REQUIRED_PARAMETERS_MISSING);
		}
		AsynDownload asynDownload = asynDownloadMapper.selectByPrimaryKey(vo.getId());
		if (CommonUtil.isEmpty(asynDownload)) {
			throw ServiceException.warning(ErrorCode.DATA_NOT_FOUND);
		}

		asynDownload.setFileStatus(vo.getAsynDownloadUploadStatu().name());
		asynDownloadMapper.updateByPrimaryKey(asynDownload);
	}

	@Override
	public void deleteAsynDownloadTask(List<Long> asynDownloadIds) {
		if (CommonUtil.isEmpty(asynDownloadIds)) {
			throw ServiceException.warning(ErrorCode.REQUIRED_PARAMETERS_MISSING);
		}

		asynDownloadIds.parallelStream().forEach(asynDownloadId ->{
			
			AsynDownload asynDownload = asynDownloadMapper.selectByPrimaryKey(asynDownloadId);
			if (CommonUtil.isEmpty(asynDownload)) {
				return ;
			}

			asynDownload.setStatus(DataStatusDef.DISABLE.name());
			asynDownloadMapper.updateByPrimaryKeySelective(asynDownload);
		});
	}

	@Override
	public AsynDownloadVo getAsynDownload(Long asynDownloadId) {
		if (CommonUtil.isEmpty(asynDownloadId)) {
			throw ServiceException.warning(ErrorCode.REQUIRED_PARAMETERS_MISSING);
		}

		AsynDownload asynDownload = asynDownloadMapper.selectByPrimaryKey(asynDownloadId);
		if (CommonUtil.isEmpty(asynDownload)) {
			throw ServiceException.warning(ErrorCode.DATA_NOT_FOUND);
		}

		AsynDownloadVo vo = new AsynDownloadVo();
		BeanUtil.copyProperties(asynDownload, vo);
		vo.setCreateTime(DateUtil.data2long(asynDownload.getCreateTime()));
		return vo;
	}

	@Override
	public AsynDownloadVo getAsynDownloadByHashCode(Long createUser, String hashCode) {
		if (CommonUtil.isAnyEmpty(createUser, hashCode)) {
			throw ServiceException.warning(ErrorCode.REQUIRED_PARAMETERS_MISSING);
		}

		AsynDownload query = new AsynDownload();
		query.setCreateUser(createUser);
		query.setHashCode(hashCode);
		AsynDownload asynDownload = asynDownloadMapper.selectOne(query);

		if (CommonUtil.isEmpty(asynDownload)) {
			throw ServiceException.warning(ErrorCode.DATA_NOT_FOUND);
		}

		AsynDownloadVo vo = new AsynDownloadVo();
		BeanUtil.copyProperties(asynDownload, vo);
		vo.setCreateTime(DateUtil.data2long(asynDownload.getCreateTime()));
		return vo;
	}

}
