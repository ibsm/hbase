package com.hm.base.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hm.base.auto.su.BeanUtil;
import com.hm.base.def.RegionTypeEnum;
import com.hm.base.domain.DscRegion;
import com.hm.base.mapper.DscRegionMapper;
import com.hm.base.service.DscRegionService;
import com.hm.base.vo.DscRegionVo;

/**
 * @author shishun.wang
 * @date 上午11:00:07 2017年8月7日
 * @version 1.0
 * @describe
 */
@Service
public class DscRegionServiceImpl implements DscRegionService {

	@Autowired
	private DscRegionMapper dscRegionMapper;

	@Override
	public List<DscRegionVo> loadDscRegions(Long parentId, RegionTypeEnum regionType) {
		List<DscRegionVo> vos = new ArrayList<DscRegionVo>();

		DscRegion region = new DscRegion();
		region.setParentId(parentId);
		region.setRegionType(regionType.status());
		
		dscRegionMapper.select(region).forEach(reg -> {
			DscRegionVo vo = new DscRegionVo();
			BeanUtil.copyProperties(reg, vo);
			vo.setRegionTypeEnum(RegionTypeEnum.trance(reg.getRegionType()));
			vos.add(vo);
		});

		return vos;
	}

}
