package com.hm.base.websocket;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.hm.base.auto.SystemEnvConfigProperties;

/**
 * @author shishun.wang
 * @date 下午3:30:23 2017年9月11日
 * @version 1.0
 * @describe
 */
@Component
public class LocalRunningInstanceLog extends TextWebSocketHandler {

	@Autowired
	private SystemEnvConfigProperties systemEnvConfigProperties;

	@Override
	protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
		Process process = Runtime.getRuntime()
				.exec("tailf -100 " + systemEnvConfigProperties.getServerInstancePath() + "/logs/hbase.log");
		BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		String line = null;
		while ((line = reader.readLine()) != null) {
			session.sendMessage(new TextMessage(line + "</br>"));
		}
	}
}
