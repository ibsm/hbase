/*
Navicat MySQL Data Transfer

Source Server         : 115.28.66.183
Source Server Version : 50552
Source Host           : 115.28.66.183:3306
Source Database       : hmbase

Target Server Type    : MYSQL
Target Server Version : 60099
File Encoding         : 65001

Date: 2017-07-28 17:45:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `hm_menu`
-- ----------------------------
DROP TABLE IF EXISTS `hm_menu`;
CREATE TABLE `hm_menu` (
`id`  bigint(11) NOT NULL AUTO_INCREMENT COMMENT '编号ID' ,
`name`  varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '菜单名称' ,
`note`  varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注描述' ,
`uri`  varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '请求地址' ,
`ico`  varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标' ,
`parent_id`  bigint(11) UNSIGNED ZEROFILL NOT NULL COMMENT '上级id' ,
`sort`  bigint(11) NULL DEFAULT NULL COMMENT '排列顺序' ,
`create_user`  bigint(11) NULL DEFAULT NULL COMMENT '创建人' ,
`create_time`  datetime NULL DEFAULT NULL COMMENT '创建时间' ,
`status`  varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '使用状态' ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=9

;

-- ----------------------------
-- Records of hm_menu
-- ----------------------------
BEGIN;
INSERT INTO `hm_menu` VALUES ('1', '我的控制台', '欢迎页', '#!workbench.html', 'fa fa-dashboard', '00000000000', '1', '1', '2017-06-05 14:32:00', 'ENABLE'), ('2', '基础信息', '基础信息', '#!', 'fa fa-dashboard', '00000000000', '2', '1', '2017-06-05 14:32:00', 'ENABLE'), ('3', '用户管理', '用户管理', '#!view/subscriber.html', 'fa fa-dashboard', '00000000002', '1', '1', '2017-06-05 14:32:00', 'ENABLE'), ('4', '角色管理', '角色管理', '#!view/role.html', 'fa fa-dashboard', '00000000002', '2', '1', '2017-06-05 14:32:00', 'ENABLE'), ('5', '菜单管理', '菜单管理', '#!view/menu.html', 'fa fa-dashboard', '00000000002', '3', '1', '2017-06-05 14:32:00', 'ENABLE'), ('6', '组织管理', '组织管理', '#!view/menu.html', 'fa fa-dashboard', '00000000002', '4', '1', '2017-06-05 14:32:00', 'ENABLE'), ('7', '系统管理', '系统管理', '#!view/menu.html', 'fa fa-dashboard', '00000000000', '3', '1', '2017-06-05 14:32:00', 'ENABLE'), ('8', '字典管理', '字典管理', '#!view/menu.html', 'fa fa-dashboard', '00000000007', '1', '1', '2017-06-05 14:32:00', 'ENABLE');
COMMIT;

-- ----------------------------
-- Table structure for `hm_organization`
-- ----------------------------
DROP TABLE IF EXISTS `hm_organization`;
CREATE TABLE `hm_organization` (
`id`  bigint(11) NOT NULL AUTO_INCREMENT COMMENT '编号ID' ,
`name`  varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '组织名称' ,
`code`  varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '组织机构码' ,
`note`  varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组织描述' ,
`parent_id`  bigint(11) UNSIGNED ZEROFILL NOT NULL COMMENT '上级组织' ,
`create_time`  datetime NOT NULL COMMENT '创建时间' ,
`create_user`  bigint(11) NOT NULL COMMENT '创建人' ,
`sort`  bigint(11) NULL DEFAULT NULL COMMENT '排列顺序' ,
`status`  varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '使用状态' ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of hm_organization
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `hm_organization_role`
-- ----------------------------
DROP TABLE IF EXISTS `hm_organization_role`;
CREATE TABLE `hm_organization_role` (
`id`  bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID' ,
`organization_id`  bigint(20) NOT NULL COMMENT '组织ID' ,
`role_id`  bigint(20) NOT NULL COMMENT '角色ID' ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of hm_organization_role
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `hm_role`
-- ----------------------------
DROP TABLE IF EXISTS `hm_role`;
CREATE TABLE `hm_role` (
`id`  bigint(11) NOT NULL AUTO_INCREMENT COMMENT '编号ID' ,
`name`  varchar(34) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名称' ,
`note`  varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注描述' ,
`create_time`  datetime NULL DEFAULT NULL COMMENT '创建时间' ,
`create_user`  bigint(11) NULL DEFAULT NULL COMMENT '创建人' ,
`status`  varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '使用状态' ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=2

;

-- ----------------------------
-- Records of hm_role
-- ----------------------------
BEGIN;
INSERT INTO `hm_role` VALUES ('1', '管理员', '超级管理员', '2017-06-05 14:32:34', '1', 'ENABLE');
COMMIT;

-- ----------------------------
-- Table structure for `hm_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `hm_role_menu`;
CREATE TABLE `hm_role_menu` (
`id`  bigint(11) NOT NULL AUTO_INCREMENT COMMENT '编号ID' ,
`menu_id`  bigint(11) NOT NULL COMMENT '菜单ID' ,
`role_id`  bigint(11) NOT NULL COMMENT '角色ID' ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=10

;

-- ----------------------------
-- Records of hm_role_menu
-- ----------------------------
BEGIN;
INSERT INTO `hm_role_menu` VALUES ('1', '1', '1'), ('2', '1', '1'), ('3', '2', '1'), ('4', '3', '1'), ('5', '4', '1'), ('6', '5', '1'), ('7', '6', '1'), ('8', '7', '1'), ('9', '8', '1');
COMMIT;

-- ----------------------------
-- Table structure for `hm_subscriber`
-- ----------------------------
DROP TABLE IF EXISTS `hm_subscriber`;
CREATE TABLE `hm_subscriber` (
`id`  bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'ID' ,
`account`  varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '账户' ,
`email`  varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱地址' ,
`user_name`  varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名' ,
`user_pwd`  varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码' ,
`phone`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电话号码' ,
`sex`  varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别' ,
`head_portrait`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像' ,
`lost_login_ip`  varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '上次登陆ip地址' ,
`last_login_time`  datetime NOT NULL COMMENT '上次登录时间' ,
`create_time`  datetime NOT NULL COMMENT '创建时间' ,
`status`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '使用状态' ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
COMMENT='登陆账户管理'
AUTO_INCREMENT=13

;

-- ----------------------------
-- Records of hm_subscriber
-- ----------------------------
BEGIN;
INSERT INTO `hm_subscriber` VALUES ('1', 'admin', '1058300241@qq.com', '三月@雪', 'd6bf4bb9a66419380a7e8b034270d381', '15884507258', 'MAN', 'http://ool4tm4nb.bkt.clouddn.com/system-manage-portrait.jpg', '127.0.0.1', '2017-06-02 17:10:29', '2017-06-02 17:10:29', 'ENABLE'), ('2', '1', '1058300241@qq.com', '三月@雪', null, '15884507258', 'MAN', 'http://ool4tm4nb.bkt.clouddn.com/system-manage-portrait.jpg', '127.0.0.1', '2017-06-02 17:10:29', '2017-06-02 17:10:29', 'ENABLE'), ('3', '2', '1058300241@qq.com', '三月@雪', null, '15884507258', 'MAN', 'http://ool4tm4nb.bkt.clouddn.com/system-manage-portrait.jpg', '127.0.0.1', '2017-06-02 17:10:29', '2017-06-02 17:10:29', 'ENABLE'), ('4', '3', '1058300241@qq.com', '三月@雪', null, '15884507258', 'MAN', 'http://ool4tm4nb.bkt.clouddn.com/system-manage-portrait.jpg', '127.0.0.1', '2017-06-02 17:10:29', '2017-06-02 17:10:29', 'ENABLE'), ('5', '4', '1058300241@qq.com', '三月@雪', null, '15884507258', 'MAN', 'http://ool4tm4nb.bkt.clouddn.com/system-manage-portrait.jpg', '127.0.0.1', '2017-06-02 17:10:29', '2017-06-02 17:10:29', 'ENABLE'), ('6', '5', '1058300241@qq.com', '三月@雪', null, '15884507258', 'MAN', 'http://ool4tm4nb.bkt.clouddn.com/system-manage-portrait.jpg', '127.0.0.1', '2017-06-02 17:10:29', '2017-06-02 17:10:29', 'ENABLE'), ('7', '6', '1058300241@qq.com', '三月@雪', null, '15884507258', 'MAN', 'http://ool4tm4nb.bkt.clouddn.com/system-manage-portrait.jpg', '127.0.0.1', '2017-06-02 17:10:29', '2017-06-02 17:10:29', 'ENABLE'), ('8', '7', '1058300241@qq.com', '三月@雪', null, '15884507258', 'MAN', 'http://ool4tm4nb.bkt.clouddn.com/system-manage-portrait.jpg', '127.0.0.1', '2017-06-02 17:10:29', '2017-06-02 17:10:29', 'ENABLE'), ('9', '8', '1058300241@qq.com', '三月@雪', null, '15884507258', 'MAN', 'http://ool4tm4nb.bkt.clouddn.com/system-manage-portrait.jpg', '127.0.0.1', '2017-06-02 17:10:29', '2017-06-02 17:10:29', 'ENABLE'), ('10', '9', '1058300241@qq.com', '三月@雪', null, '15884507258', 'MAN', 'http://ool4tm4nb.bkt.clouddn.com/system-manage-portrait.jpg', '127.0.0.1', '2017-06-02 17:10:29', '2017-06-02 17:10:29', 'ENABLE'), ('11', '10', '1058300241@qq.com', '三月@雪', null, '15884507258', 'MAN', 'http://ool4tm4nb.bkt.clouddn.com/system-manage-portrait.jpg', '127.0.0.1', '2017-06-02 17:10:29', '2017-06-02 17:10:29', 'ENABLE'), ('12', '11', '1058300241@qq.com', '三月@雪', null, '15884507258', 'MAN', 'http://ool4tm4nb.bkt.clouddn.com/system-manage-portrait.jpg', '127.0.0.1', '2017-06-02 17:10:29', '2017-06-02 17:10:29', 'ENABLE');
COMMIT;

-- ----------------------------
-- Table structure for `hm_subscriber_organization`
-- ----------------------------
DROP TABLE IF EXISTS `hm_subscriber_organization`;
CREATE TABLE `hm_subscriber_organization` (
`id`  bigint(11) NOT NULL AUTO_INCREMENT COMMENT '编号ID' ,
`user_id`  bigint(11) NOT NULL COMMENT '用户ID' ,
`organization_id`  bigint(11) NOT NULL COMMENT '组织ID' ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=1

;

-- ----------------------------
-- Records of hm_subscriber_organization
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for `hm_subscriber_role`
-- ----------------------------
DROP TABLE IF EXISTS `hm_subscriber_role`;
CREATE TABLE `hm_subscriber_role` (
`id`  bigint(11) NOT NULL AUTO_INCREMENT COMMENT '编号ID' ,
`subscriber_id`  bigint(11) NOT NULL COMMENT '用户ID' ,
`role_id`  bigint(11) NOT NULL COMMENT '角色ID' ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=3

;

-- ----------------------------
-- Records of hm_subscriber_role
-- ----------------------------
BEGIN;
INSERT INTO `hm_subscriber_role` VALUES ('1', '1', '1'), ('2', '1', '1');
COMMIT;

-- ----------------------------
-- Table structure for `hm_system_config_dict`
-- ----------------------------
DROP TABLE IF EXISTS `hm_system_config_dict`;
CREATE TABLE `hm_system_config_dict` (
`id`  bigint(11) NOT NULL AUTO_INCREMENT COMMENT '编号ID' ,
`type`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典分类' ,
`name`  varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典名称' ,
`code`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典CODE' ,
`value`  varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '字典VALUE' ,
`sort`  int(5) NOT NULL COMMENT '排列顺序' ,
`create_time`  datetime NOT NULL COMMENT '创建时间' ,
`create_user`  bigint(11) NULL DEFAULT NULL COMMENT '创建人' ,
`status`  varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数据状态' ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=utf8 COLLATE=utf8_general_ci
AUTO_INCREMENT=5

;

-- ----------------------------
-- Records of hm_system_config_dict
-- ----------------------------
BEGIN;
INSERT INTO `hm_system_config_dict` VALUES ('1', 'SEX', '男', 'MAN', 'MAN', '1', '2017-06-14 11:29:13', '1', 'ENABLE'), ('2', 'SEX', '女', 'WOMAN', 'WOMAN', '2', '2017-06-14 11:30:42', '1', 'ENABLE'), ('3', 'MARRY', '已婚', 'MARRY_YES', 'MARRY_YES', '1', '2017-06-14 11:31:53', '1', 'ENABLE'), ('4', 'MARRY', '未婚', 'MARRY_NO', 'MARRY_NO', '2', '2017-06-14 11:32:12', '1', 'ENABLE');
COMMIT;

-- ----------------------------
-- Auto increment value for `hm_menu`
-- ----------------------------
ALTER TABLE `hm_menu` AUTO_INCREMENT=9;

-- ----------------------------
-- Auto increment value for `hm_organization`
-- ----------------------------
ALTER TABLE `hm_organization` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `hm_organization_role`
-- ----------------------------
ALTER TABLE `hm_organization_role` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `hm_role`
-- ----------------------------
ALTER TABLE `hm_role` AUTO_INCREMENT=2;

-- ----------------------------
-- Auto increment value for `hm_role_menu`
-- ----------------------------
ALTER TABLE `hm_role_menu` AUTO_INCREMENT=10;

-- ----------------------------
-- Auto increment value for `hm_subscriber`
-- ----------------------------
ALTER TABLE `hm_subscriber` AUTO_INCREMENT=13;

-- ----------------------------
-- Auto increment value for `hm_subscriber_organization`
-- ----------------------------
ALTER TABLE `hm_subscriber_organization` AUTO_INCREMENT=1;

-- ----------------------------
-- Auto increment value for `hm_subscriber_role`
-- ----------------------------
ALTER TABLE `hm_subscriber_role` AUTO_INCREMENT=3;

-- ----------------------------
-- Auto increment value for `hm_system_config_dict`
-- ----------------------------
ALTER TABLE `hm_system_config_dict` AUTO_INCREMENT=5;
